<?php
/**
 * @file
 * Advimage and advupload  integration module
 * JSON menu calls
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */

/*
 * JSON call to add image to the gallery on site
 */
function advimage_up_addgallery_json() {
  global $user;
  module_load_include('inc', 'node', 'node.pages');
  drupal_set_header('Content-type: text/javascript; charset=utf-8');
  drupal_set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  drupal_set_header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header('Cache-Control: no-store, no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');
  // checking arg(0) arg(1)
  $tokens_correct = false;
  $token_name = arg(2);
  $token = arg(3);
  if ($token_name && $token) {
		$tokens_correct = drupal_valid_token($token, $token_name);
	}
  if( !$tokens_correct ) {
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Something wrong with form sessions. Try make all upload operations again. If error repeats tell about it to site administrator')
		) ) );
	}
	$title = isset($_POST['advimage_up_advgallery'] ) ? $_POST['advimage_up_advgallery'] : '';
	if (empty($title) ) {
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Inapropriate javascript call without galery title.')
		) ) );
	}
	$node        = new stdClass();
	$node->status = 1;
  $node->type  = 'advgallery';
  $node->uid   = $user->uid;
  $node->name  = $user->name;
  $node->title = $title;
  $node->body  = '';
  node_object_prepare($node);
  $node->form_id = $node->type .'_node_form';
  $node = node_submit($node);
	// try to use advupload_og module hook
	if (
		module_exists('advupload_og') 
		&& function_exists('advupload_og_advupload_prenodesave') 
		&& isset($_POST['node_og_public'] )
		&& isset($_POST['node_og_groups_string'] )
		&& isset($_POST['node_og_public_string'] )
	) {
		$addfields = array(
			'og_public' => $_POST['node_og_public'],
			'og_groups_string' => $_POST['node_og_groups_string'],
			'og_public_string' => $_POST['node_og_public_string']
		);
		advupload_og_advupload_prenodesave($node, $addfields);
	}
	node_save($node);
	$token_name = 'advimage_up_' . str_replace('.', '_', uniqid('', TRUE));
	// add internal node salt there
	$token = drupal_get_token(
		$token_name 
		. '_' . $node->nid 
		. '_'  . $node->created
	);
	if ($node->nid) {
		$data = array(
			'node_advimage_up_nid' => $node->nid, 
			'title' => $node->title,
			'url' => url('node/' . $node->nid, array('absolute' => TRUE) ),
			'node_advimage_up_advgallery_token_name' => $token_name,
			'node_advimage_up_advgallery_token' => $token,
			'html' => theme('advupload_json_edit_node_draw', $node->nid)
		);
		$result = array('jsonrpc' => '2.0', 'result' => 'success', 'data' => $data);
		die(drupal_to_js($result) );
	}
	die(drupal_to_js(array(
		'jsonrpc' => '2.0', 'result' => 'error',
		'message' => t('Unknown error while saving gallery node')
	) ) );;
}
