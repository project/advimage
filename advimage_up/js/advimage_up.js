/**
 * @file
 * JS for advanced image advanced upload imtegration module, 
 * for advanced upload page
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
Drupal = Drupal || {};
Drupal.advimage_up =  Drupal.advimage_up || {};
Drupal.advimage_up = {
  _whileremoving : false,
  advgallery_data : undefined,
  _beforeupload_firstcall : true,
  uploader_FilesAdded : function(up) {
    if(Drupal.advimage_up._whileremoving) return;
    if(
      ($('#edit-advimage-up-advgallery').length == 1)
      && ($('#edit-advimage-up-advgallery').val() != '') 
    ){
      var maxcount = Drupal.settings.advimage_up.advimage_gallery_max_images_in_gallery;
      if(maxcount < up.files.length) {
        // remove files
        Drupal.advimage_up._whileremoving = true;
        while(up.files.length > maxcount) {
          up.removeFile(up.files[up.files.length - 1] );
        }
        Drupal.advimage_up._whileremoving = false;
        alert(
          Drupal.t('Maximum number of images in the gallery is: ') 
          + maxcount + "\n" 
          + Drupal.t('That\'s why you cannit upload more images if you want add them all to new gallery. Some last added images were removed from the form')
        );
      }
    }
  },
  addGalleryJsonCall : function (result) {
    var response = eval('(' + result + ')');
    if( 
      (response.result == undefined) 
      || (response.result == 'error')  
      || ( (response.result == 'success') && (response.data == 'undefined') )
    ) {
      var errormessage = Drupal.t('Unknown error occured wile creating gallery. Please reload form. If error repeats please tell about it to site administrator');
      if(response.message != undefined) {
        errormessage = response.message;
      }
      // stopping advupload
      $('#advupload-uploader-input-values').hide();
      $('#advupload-uploader').hide();
      $('#advupload-uploader-wrapper').prepend('<div id="advupload-uploader-stop-error"></div>');
      $('#advupload-uploader-stop-error').text(errormessage);
      $('#advupload-uploader').advupload_pluploadQueue().stop();
      return;
    }
    if(response.result == 'success'){
      $.extend(
        $('#advupload-uploader').advupload_pluploadQueue().settings.multipart_params,
        {
          node_advimage_up_nid : response.data.node_advimage_up_nid,
          node_advimage_up_advgallery_token_name : response.data.node_advimage_up_advgallery_token_name,
          node_advimage_up_advgallery_token : response.data.node_advimage_up_advgallery_token
        }
      );
      Drupal.advimage_up.advgallery_data = response.data;
    }
  },
  /*
   * Fires before each file upload
   */
  uploader_BeforeUpload : function(up, files) {
    if(Drupal.advimage_up._beforeupload_firstcall){
      Drupal.advimage_up._beforeupload_firstcall = false;
      var galname = $('#edit-advimage-up-advgallery').length ? $('#edit-advimage-up-advgallery').val() : '';
      if( 
        (typeof(galname) != 'undefined') &&
        (galname != '')
      ) {
        // !!!! TODO: find additional form input values and send there them too
        // (we may loose opengroups inputs ))
        var inputs = $('#advupload-uploader-input-values-modules input');
        var ivalues = {};
        var evalstr = '';
        for(i = 0; i < inputs.length; i++) {
          ivalues[inputs[i].name] = inputs[i].value;
        }
        $.ajax({
          url : Drupal.settings.advimage_up.gallery_save_url,
          type : 'POST',
          data : ivalues,
          success : Drupal.advimage_up.addGalleryJsonCall,
          async : false
        });
      }
    }
  },
  uploader_UploadComplete : function(up, files) {
    if(Drupal.advimage_up.advgallery_data != undefined) {
      $('#advupload-uploader-hook-pre').append(
        '<h3>' 
        + Drupal.t('New image gallery was created')
        + '</h3>'
      );
      $('#advupload-uploader-hook-pre').append(
        Drupal.advimage_up.advgallery_data.html
      );
      if(files.length > 1) {
        $(location).attr('href', Drupal.advimage_up.advgallery_data.url);
      }
    }
  },
  waitForUploaderReady : function() {
    if(
      ('undefined' == typeof($('#advupload-uploader').advupload_pluploadQueue) )
      || ('undefined' == typeof($('#advupload-uploader').advupload_pluploadQueue() ) )
    ) {
      setTimeout(Drupal.advimage_up.waitForUploaderReady, 10);
      return false;
    }
    var up = $('#advupload-uploader').advupload_pluploadQueue();
    up.bind('FilesAdded', Drupal.advimage_up.uploader_FilesAdded);
    up.bind('BeforeUpload', Drupal.advimage_up.uploader_BeforeUpload);
    up.bind('UploadComplete', Drupal.advimage_up.uploader_UploadComplete);
    return true;
  }
}
$(document).ready(function(){
  Drupal.advimage_up.waitForUploaderReady();
});
