<?php
/**
 * @file
 * Additional API for advimage module
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
 
/*
 * Return node info for standart drupal functions
 */
function _advimage_default_node_info(){
  $types = array(
    'advimage' => array(
      'name' => t('Image gallery entry - single image'),
      'module' => 'advimage',
      'description' => t('A document containing image for the Advanced Gallery module'),
      'body_label' => t('Your description for the image'),
      'has_title' => TRUE,
      'title_label' => t('Title'),
      'has_body' => TRUE,
    ),
    'advgallery' => array(
      'name' => t('Image gallery'),
      'module' => 'advimage',
      'description' => t('Image gallery'),
      'body_label' => t('Your description for this gallery'),
      'has_title' => TRUE,
      'title_label' => t('Title'),
      'has_body' => TRUE,
    ),
  );
  return $types;
}
/*
 * Return count of images in the gallery user have access to
 */
function _advimage_advgallery_get_secure_images_count($node, $flush =  false) {
  static $storage;
  if (!isset($storage) ) {
    $storage = array();
  }
  if ($flush) {
    unset($storage);
    $storage = array();
  }
  if(isset($storage[$node->nid] ) ) return $storage[$node->nid];
  $result = db_query(db_rewrite_sql(
      'SELECT 
   count(DISTINCT(n.nid) ) AS cc
FROM {content_field_advgallery} ag
INNER JOIN {node} n ON ag.field_advgallery_nid=n.nid
INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE ag.nid=%d AND ag.vid=%d AND n.uid=%d AND n.type=\'advimage\' AND n.status=1  ORDER BY ag.delta, n.created DESC'
    ),
    $node->nid, $node->vid, $node->uid
  );
  if($row = db_fetch_array($result) ) {
    $storage[$node->nid] = $row['cc'];
    return $storage[$node->nid];
  }
  return 0;
}
/*
 * Returns list of images for current gallery according to different drupal access rules
 * e.g. using db_rewrite_sql() call
 */
function _advimage_advgallery_get_secure_images_list($node, $paging = FALSE, $page = 0, $page_num = ADVIMAGE_GALLERY_FULL_PAGE_NUM) {
  if ($paging) {
    $startimage = $page * $page_num;
    $result = db_query_range(db_rewrite_sql(
        'SELECT 
   DISTINCT(n.nid) AS nid, 
   n.created, n.vid, n.title, ag.delta, f.fid, f.filename, f.filepath, f.filemime, f.filesize
FROM {content_field_advgallery} ag
INNER JOIN {node} n ON ag.field_advgallery_nid=n.nid
INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE ag.nid=%d AND ag.vid=%d AND n.uid=%d AND n.type=\'advimage\' AND n.status=1 ORDER BY ag.delta, n.created DESC'
      ),
      $node->nid, $node->vid, $node->uid,
      $startimage, $page_num
    );
  } else {
    $startimage = 0;
    $result = db_query(db_rewrite_sql(
        'SELECT 
   DISTINCT(n.nid) AS nid, 
   n.created, n.vid, n.title, ag.delta, f.fid, f.filename, f.filepath, f.filemime, f.filesize
FROM {content_field_advgallery} ag
INNER JOIN {node} n ON ag.field_advgallery_nid=n.nid
INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE ag.nid=%d AND ag.vid=%d AND n.uid=%d AND n.type=\'advimage\' AND n.status=1  ORDER BY ag.delta, n.created DESC'
      ),
      $node->nid, $node->vid, $node->uid
    );
  }
  $images = array();
  while ($row = db_fetch_array($result) ) {
    $row['onepagenum'] = $startimage;
    $images []= $row;
    $startimage ++;
  }
  return $images;
}

function _advimage_advgallery_get_secure_images_pager_query($node, $page_num = ADVIMAGE_GALLERY_FULL_PAGE_NUM, $pager_element = 4) {
  $result = pager_query(db_rewrite_sql(
      'SELECT 
 DISTINCT(n.nid) AS nid, 
 n.created, n.vid, n.title, ag.delta, f.fid, f.filename, f.filepath, f.filemime, f.filesize
FROM {content_field_advgallery} ag
INNER JOIN {node} n ON ag.field_advgallery_nid=n.nid
INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE ag.nid=' 
  . intval($node->nid) . ' AND ag.vid=' 
  . intval($node->vid) . ' AND n.uid=' . intval($node->uid) 
  . ' AND n.type=\'advimage\' AND n.status=1 ORDER BY ag.delta, n.created DESC'
    ),
    $page_num, $pager_element, NULL
  );
  $images = array();
  while ($row = db_fetch_array($result) ) {
    $images []= $row;
  }
  return $images;
}
