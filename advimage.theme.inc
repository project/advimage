<?php
/**
 * @file
 * Theme functions
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
/*
 * Custom readmore hook - think it's about themization e.g. for non default behaviour of galleries
 * returns true or false value :)
 */
function theme_advimage_view_advgallery_own_readmore_value($node) {
  return (
    isset($node->field_advgallery) 
    && is_array($node->field_advgallery) 
    && (count($node->field_advgallery) > ADVIMAGE_GALLERY_TEASER_PAGE_NUM)
    && ( (!isset($node->readmore) ) || (!$node->readmore) ) 
  );
}
/*
 * Displays list of galleries node is added to
 */
function theme_advimage_advimage_where_in_galleries($node) {
  $result = db_query(db_rewrite_sql(
    'SELECT n.nid, n.title 
FROM {node} n INNER JOIN {content_field_advgallery} ag ON n.nid=ag.nid AND n.vid=ag.vid
WHERE n.status=1 AND n.type=\'advgallery\' AND ag.field_advgallery_nid=%d ORDER BY n.created DESC'
    ),
    $node->nid
  );
  $galleries = array();
  while ($row = db_fetch_array($result) ) {
    $galleries []= $row;
  }
  if(count($galleries) ) {
    $plural = (count($galleries) > 1);
    $out = '<div  class="advimage-advimage-where-in-galleries">';
    $out .= $plural ? t('This image is in gallery:') : t('This image is in galleries:');
    $out .= ' ';
    $first =  TRUE;
    foreach ($galleries as $g) {
      $out .= ($first ? '' : ', ') . l($g['title'], 'node/' . $g['nid']);
      $first = FALSE;
    }
    $out .= '</div>';
    return $out;
  }
  return '';
}
/*
 * Displays single image miniiature in list of miniatures on single image gallery page
 * $gnode - gallery node, $inode - current image node, $image- loaded images structure
 * to display
 */
function theme_advimage_gallery_view_mini_image($gnode, $inode, $image) {
  $current = ($inode->nid == $image['nid']);
  return l(
    '<img src="' 
      . imagecache_create_url('advimage_mini', $image['filepath'] ) 
      . '" alt="' . check_plain($image['title'] ) . '" title="'
      . check_plain($image['title'] ) . '" />',
    'node/' . $gnode->nid . '/advgallery/' . $image['nid'],
    array(
      'html' => TRUE,
      'attributes' => array(
        'class' => 'advimage-gallery-view-mini-image ' 
          . ($current ? 'current' : '')
      )
    )
  );
}
/*
 * Displays list of miniatures on single image gallery page
 * $gnode - gallery node, $inode - current image node, $images - loaded images structures array
 */
function theme_advimage_gallery_view_mini_list($gnode, $inode, $images) {
  $out = '<table class="advimage-gallery-view-mini-list">';
  $c = count($images);
  $strc = 5;
  $c1 = floor($c/$strc) +  ( ( ($c % $strc) > 0 ) ? 1 : 0);
  for ($i =  0; $i < $c1; $i ++) {
    $out .= '<tr>';
    $j1 = $i * $strc;
    $j2 = ($i + 1) * $strc;
    if($j2 > $c) $j2 = $c;
    for ($j = $j1; $j < $j2; $j ++) {
      $current = ($images[$j] ['nid'] == $inode->nid);
      $out .= '<td class="advimage-gallery-view-mini-td ' . ($current ? 'current' : '') . '">';
      $out .= theme('advimage_gallery_view_mini_image', $gnode, $inode, $images[$j] );
      $out .= '</td>';
    }
    $out .= '</tr>';
  }
  $out .= '</table>';
  return $out;
}
/*
 * Displays gallery single image page.
 * $gnode - gallery parent node, $inode - image child node
 */
function theme_advimage_gallery_view($gnode, $inode) {
  global $user;
  module_load_include('inc', 'advimage', 'advimage.api');
  $out = '<h2 class="advimage-image-view title">' . check_plain($inode->title) . '</h2>';
  $images = _advimage_advgallery_get_secure_images_list($gnode, FALSE);
  // search $inode there
  $prev = NULL;$next =  NULL;$first = $inode->nid;
  $imgfound =  FALSE;
  $image = NULL;
  if (count($images) ) {
    for ($i = 0; $i < count($images); $i ++) {
      if($images[$i]['nid'] == $inode->nid) {
        $imgfound = TRUE;
        $image = $images[$i];
        if ($i > 0) {
          $prev = $images[$i - 1] ['nid'];
        }
        if ($i < (count($images) - 1) ) {
          $next = $images[$i + 1] ['nid'];
        }
      }
    }
    $first = $images[0] ['nid'];
  }
  if ($imgfound) {
    $out .= '<table class="advimage-image-view"><tr>';
    $out .= '<td class="navlink left">';
    if ($prev === NULL)  {
      $out .= '<span class="navlink disabled">&lt;&lt;</span>';
    } else {
      $out .= '<span class="navlink">'
        . l('<<', 'node/' . $gnode->nid . '/advgallery/' . $prev)
        . '</span>';
    }
    $out .= '</td>';
    $out .= '<td class="advimage-image">';
    $out .= l(
      '<img src="' 
        . imagecache_create_url('advimage_view1', $image['filepath'] ) 
        . '" title="' . check_plain($image['title'] ) . '" alt="'
        . check_plain($image['title'] ) . '" />',
      'node/' . $gnode->nid . '/advgallery/' 
        . ( ($next === NULL) ? $first : $next ),
      array('html' => TRUE) 
    );
    $out .= '<br />';
    $out .= l(t('View full size'), $image['filepath'], array('attributes' => array('class' => 'fullsize-link') ) );
    $out .= '</td>';
    $out .= '<td class="navlink right">';
    if ($next === NULL)  {
      $out .= '<span class="navlink disabled">&gt;&gt;</span>';
    } else {
      $out .= '<span class="navlink">'
        . l('>>', 'node/' . $gnode->nid . '/advgallery/' . $next)
        . '</span>';
    }
    $out .= '</td>';
    $out .= '</tr></table>';
    // list of other gallery images
    $out .= theme('advimage_gallery_view_mini_list', $gnode, $inode, $images);
  } else {
    $out .= '<div class="advimage-image-notfound">' . t('Error: There was no image uploaded to this node') . '</div>';
  }
  $out .= '<h3 class="title">Additional information</h3>';
  $out .= '<div class="advimage-image-additional">' 
    . t('Link to image node with this image: ')
    . l(url('node/' . $inode->nid, array('absolute' => TRUE) ), 'node/' . $inode->nid)
    . '</div>';
  return $out;
}
/*
 * Displays image preview in the preview list for image struct on gallery page
 */
function theme_advimage_view_advgallery_image_thumbnail($node, $image, $teaser) {
  return l(
    '<img src="' . imagecache_create_url('advimage_thumb1', $image['filepath'] ) 
      . '" title="' . $image['title'] . '" />',
    $teaser ? 
      ('node/' . $node->nid) 
      : ('node/' . $node->nid . '/advgallery/' . $image['nid']),
    array('html' => TRUE)
  );
}
/*
 * Displays thumbnail text of an image
 */
function theme_advimage_view_advgallery_image_thumbnail_text($node, $image, $teaser) {
  return l(
    $image['title'],
    $teaser ? 
      ('node/' . $node->nid) 
      : ('node/' . $node->nid . '/advgallery/' . $image['nid'])
  );
}
function theme_advimage_view_advgallery_thumbnails($node, $images, $teaser) {
  $out = '<table class="advimage-thumbnails">';
  // go $images by 3 and put to table
  $c = count($images);
  $rowlength = 3;
  $rownum = floor($c / $rowlength) + ( ($c % $rowlength) ? 1 : 0);
  for ($i = 0; $i < $rownum; $i ++){
    // images
    $out .= '<tr class="advimage-thumbnails-images">';
    $j1 = $i * $rowlength;
    $j2 = ($i + 1) *$rowlength;
    if ($j2 > $c) $j2 = $c;
    for ($j = $j1; $j < $j2; $j ++) {
      $out .= '<td>';
      $out .= theme('advimage_view_advgallery_image_thumbnail', $node, $images[$j], $teaser);
      $out .= '</td>';
    }
    $out .= '</tr><tr class="advimage-thumbnails-titles">';
    for ($j = $j1; $j < $j2; $j ++) {
      $out .= '<td><span>';
      $out .= theme('advimage_view_advgallery_image_thumbnail_text', $node, $images[$j], $teaser);
      $out .= '</span></td>';
    }
    $out .= '</tr>';
  }
  $out .= '</table>';
  return $out;
}
 /*
 * Formats teaser output of advgallery field_advgallery with attached to gallery images
 */
function theme_advimage_formatter_advimage_advgallery_teaser($element) {
  global $user;
  if (isset($element['#field_name'] ) ) {
    $fieldname = $element['#field_name'];
    if (!isset($element['#node'] ) ) return '';
    $node = $element['#node'];
    $fieldvalues = $node->$fieldname;
    if(empty($node->nid) ) return '';
    $empty_galery = TRUE;
    // collect nids to count amount in fieldvalues
    if (is_array($fieldvalues) && count($fieldvalues) ) {
      // TODO !!!!!!!!!!!!!!!
      $nids = array();
      $imgs = array();
      foreach ($fieldvalues as $value) {
        if (
          is_array($value) && @is_array($value['safe']) && isset($value['safe']['nid'] )
        ) {
          if(is_numeric($value['safe']['nid'] ) ) {
            $nids []= $value['safe']['nid'] ;
            $imgs [] = $value['save'];
          }
        }
      }
      if(count($nids) ){
        /*
         * Of course you can use info from imgs to preload nodes, etc.
         * Bu WE recommend use _advimage_advgallery_get_secure_images_list() from fie advimage.api.inc
         * just because of imagelist may depend on access rights to nodes, e.g. rights from og(open groups)
         * module or some other simiar modules.
         * So we do begin!
         */
        module_load_include('inc', 'advimage', 'advimage.api');
        $images = _advimage_advgallery_get_secure_images_list($node, TRUE, 0, ADVIMAGE_GALLERY_TEASER_PAGE_NUM);
        if(count($images) ) {
          $empty_galery = FALSE;
          return theme('advimage_view_advgallery_thumbnails', $node, $images, TRUE);
        }
      }
    }
    // one else block for other cases above
    if ($empty_galery) {
      if ($node->uid == $user->uid) {
        return theme('advimage_advgallery_empty_owner', $node, TRUE);
      } else {
        return theme('advimage_advgallery_empty_user', $node, TRUE);
      }
    }
  }
  return '';
}

/*
 * Formats full output of advgallery field_advgallery with attached to gallery images
 */
function theme_advimage_formatter_advimage_advgallery_full($element) {
  global $user;
  if (isset($element['#field_name'] ) ) {
    $fieldname = $element['#field_name'];
    $node = $element['#node'];
    if (!$node->nid) return '';
    $fieldvalues = $node->$fieldname;
    $empty_galery = TRUE;
    // one else block for other cases above
    if (is_array($fieldvalues) && count($fieldvalues) ) {
      $nids = array();
      $imgs = array();
      foreach ($fieldvalues as $value) {
        if (
          is_array($value) && @is_array($value['safe']) && isset($value['safe']['nid'] )
        ) {
          if(is_numeric($value['safe']['nid'] ) ) {
            $nids []= $value['safe']['nid'] ;
            $imgs [] = $value['save'];
          }
        }
      }
      if(count($nids) ){
        /*
         * Of course you can use info from imgs to preload nodes, etc.
         * Bu WE recommend use _advimage_advgallery_get_secure_images_list() from fie advimage.api.inc
         * just because of imagelist may depend on access rights to nodes, e.g. rights from og(open groups)
         * module or some other simiar modules.
         * So we do begin!
         */
        module_load_include('inc', 'advimage', 'advimage.api');
        $page = @intval( $_GET['page'] );
        $images = _advimage_advgallery_get_secure_images_pager_query($node);
        if(count($images) ) {
          $empty_galery = FALSE;
          $out = theme('advimage_view_advgallery_thumbnails', $node, $images, FALSE) 
            . theme('pager', array(), ADVIMAGE_GALLERY_FULL_PAGE_NUM, 4 );
          return $out;
        }
      }
    }
    if ($empty_galery) {
      if ($node->uid == $user->uid) {
        return theme('advimage_advgallery_empty_owner', $node, FALSE);
      } else {
        return theme('advimage_advgallery_empty_user', $node, FALSE);
      }
    }
  }
  return '';
}

/*
 * Shows misc info neeed for owner while viewing the gallery if it's empty
 */
function theme_advimage_advgallery_empty_owner($node, $teaser = TRUE) {
  // !!!! TODO
  return t(
    'Sorry, for now your gallery is empty. But you can add images using "!link" tab',
    array('!link' => l(t('Edit gallery'), 'node/' . $node->nid .'/advimage_editgallery') )
  );
}
/*
 * Shows misc info neeed for regular user - not the gallery owner 
 * while viewing the gallery if it's empty
 */
function theme_advimage_advgallery_empty_user($node, $teaser = TRUE) {
  // !!!! TODOsh
  return t('This gallery has no images for viewing');
}
/*
 * Shows gallery teaser in list.
 */
function theme_advimage_gallery_list_teaser($node) {
  return node_view($node, TRUE);
}
/* shows image in images list !!! not while gallery render */
function theme_advimage_image_list_teaser($node) {
  return node_view($node, TRUE);
}
/*
 * Shows when user have no galleries
 */
function theme_advimage_my_galleries_list_nothing_found() {
  return '<div class="advimage-message">' 
    . t('You have not created any galleries yet') 
    . '</div>';
}
/*
 * Shows when no galleries on site
 */
function theme_advimage_all_galleries_list_nothing_found() {
  return '<div class="advimage-message">' 
    . t('No galleries was found you have access to') 
    . '</div>';
}
/*
 * Shows when user have no images on site
 */
function theme_advimage_my_images_list_nothing_found() {
  return '<div class="advimage-message">' 
    . t('You have not added any images yet.') 
    . '</div>';
}
/*
 * Shows when cannot see any images on site
 */
function theme_advimage_all_images_list_nothing_found() {
  return '<div class="advimage-message">' 
    . t('No images found you have access to') 
    . '</div>';
}
/*
 * Shows when user have no galleries
 */
function theme_advimage_user_galleries_nothing_found($account) {
  global $user;
  if ($user->uid == $account->uid) {
    return '<div class="advimage-message">' 
      . t('You currently have no galleries added')
      . '</div>';
  } else {
    return '<div class="advimage-message">' 
      . t(
        'User !user currently has no galleries added', 
        array(
          '!user' =>  theme('username', $account) 
        ) 
      ) . '</div>';
  }
}

/*
 * Shows when user have no galleries
 */
function theme_advimage_user_all_images_nothing_found($account) {
  global $user;
  if ($user->uid == $account->uid) {
    return '<div class="advimage-message">' 
      . t('You currently have no images added')
      . '</div>';
  } else {
    return '<div class="advimage-message">' 
      . t(
        'User !user currently has no images added', 
        array(
          '!user' =>  theme('username', $account) 
        ) 
      ) . '</div>';
  }
}
