<?php
/**
 * @file
 * Main module file
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
 
/*
 * Constants. For future - put them to module configuration variables
 */
/*
 * Number of images to show/upload on gallery edit pages
 */
DEFINE(ADVIMAGE_EDIT_PAGE_NUM, 10);
/*
 * Number of images per page to show on TEASER gallery view
 */
DEFINE(ADVIMAGE_GALLERY_TEASER_PAGE_NUM, 3);
/*
 * Number of images per page to show on FULL gallery view
 */
DEFINE(ADVIMAGE_GALLERY_FULL_PAGE_NUM, 102);
/*
 * Maximum amount of images in the gallery
 */
DEFINE(ADVIMAGE_GALLERY_MAX_IMAGES_IN_GALLERY, 500);
/**
 * Implementation of hook_perm().
 */
function advimage_perm() {
  return array(
    'create advgallery content',
    'delete own advgallery content',
    'delete any advgallery content',
    'edit own advgallery content',
    'edit any advgallery content',
    'create advimage content',
    'delete own advimage content',
    'delete any advimage content',
    'edit own advimage content',
    'edit any advimage content',
    'view foreign galleries list in user profile',
    'view foreign images pool list in user profile',
    'view all users images listing',
  );
}
/**
 * Implementation of hook_form().
 */
function advimage_menu() {
  $items = array();
  // my(user) not categorized photos - as default tab
  $items['advimage'] = array(
    'title' => 'Image galleries',
    'description' => 'Image galeries you have created',
    'page callback' => 'advimage_my_galleries_list',
    // use our callback cause off more complex permission check
    'access callback' => 'advimage_galleries_list_access',
    'access arguments' => array('my'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => -1,
    'file' => 'advimage.handlers.inc',
  );
  $items['advimage/mygalleries'] = array(
    'title' => 'My image galleries',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['advimage/allgalleries'] = array(
    'title' => 'Image galleries of all site users',
    'description' => 'List of galleries created by all users',
    'page callback' => 'advimage_all_galleries_list',
    // use our callback cause off more complex permission check
    'access callback' => 'advimage_galleries_list_access',
    'access arguments' => array('foreign'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
    'file' => 'advimage.handlers.inc',
  );
  $items['advimage/myimages'] = array(
    'title' => 'My images',
    'description' => 'List of my images added to the site',
    'page callback' => 'advimage_my_images_list',
    // use our callback cause off more complex permission check
    'access callback' => 'advimage_images_list_access',
    'access arguments' => array('my'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
    'file' => 'advimage.handlers.inc',
  );
  $items['advimage/allimages'] = array(
    'title' => 'All images on site',
    'description' => 'List of all site images',
    'page callback' => 'advimage_all_images_list',
     // use our callback cause off more complex permission check
    'access callback' => 'advimage_images_list_access',
    'access arguments' => array('foreign'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'advimage.handlers.inc',
  );
  $items['node/%node/advgallery/%node'] = array(
    'title' => 'Gallery view',
    'description' => 'View gallery image',
    'page callback' => 'advimage_gallery_view',
    'page arguments' => array(1, 3),
    'access callback' => 'advimage_gallery_view_access',
    'access arguments' => array(1, 3),
    'type' => MENU_CALLBACK,
    'file' => 'advimage.handlers.inc',
  );
  $items['node/%node/advimage_editgallery'] = array(
    'title' => 'Edit galery',
    'page callback' => 'advimage_edit_gallery',
    'page arguments' => array(1),
    'access callback' => 'advimage_edit_gallery_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 8,
    'file' => 'advimage.edit.inc',
  );
  $items['node/%node/advimage_editgallery/getmorefreeimages'] = array(
    'title' => 'Get more gallery images via JSON handler',
    'page callback' => 'advimage_edit_gallery_morefreeimages_json',
    'page arguments' => array(1),
    'access callback' => 'advimage_edit_gallery_access',
    'access arguments' => array(1),
    'type' => MENU_CALBACK,
    'weight' => 8,
    'file' => 'advimage.edit.inc',
  );
  $items['node/%node/advimage_editgallery/savegallery'] = array(
    'title' => 'Save gallery images with their order via JSON handler',
    'page callback' => 'advimage_edit_gallery_savegallery_json',
    'page arguments' => array(1),
    'access callback' => 'advimage_edit_gallery_access',
    'access arguments' => array(1),
    'type' => MENU_CALBACK,
    'weight' => 8,
    'file' => 'advimage.edit.inc',
  );
  $items['node/%node/advimage_editgallery/getimagebyurl'] = array(
    'title' => 'Find an image to add it to the gallery by URL via JSON handler',
    'page callback' => 'advimage_edit_gallery_getimagebyurl_json',
    'page arguments' => array(1),
    'access callback' => 'advimage_edit_gallery_access',
    'access arguments' => array(1),
    'type' => MENU_CALBACK,
    'weight' => 8,
    'file' => 'advimage.edit.inc',
  );
  $items['user/%user/advimage'] = array(
    'title' => 'Galleries and images',
    'page callback' => 'advimage_user_galleries',
    'page arguments' => array(1),
    'access callback' => 'advimage_user_galleries_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'advimage.handlers.inc',
  );
  $items['user/%user/advimage/galleries'] = array(
    'title' => 'Galleries',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['user/%user/advimage/allimages'] = array(
    'title' => 'All images',
    'page callback' => 'advimage_user_all_images',
    'page arguments' => array(1),
    'access callback' => 'advimage_user_all_images_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'advimage.handlers.inc',
  );
  return $items;
}
/**
 * Implementation of hook_node_info().
 */
function advimage_node_info() {
  module_load_include('inc', 'advimage', 'advimage.api');
  return _advimage_default_node_info();
}
/**
 * Implementation of hook_form().
 */
function advimage_form(&$node, $form_state) {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $type = node_get_types('type', $node);
  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -5
    );
  }
  if ($type->has_body) {
    // In Drupal 6, we use node_body_field() to get the body and filter
    // elements. This replaces the old textarea + filter_form() method of
    // setting this up. It will also ensure the teaser splitter gets set up
    // properly.
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }
  return $form;
}
/*
 * Implementation of hook_validate()
 */
function advimage_validate($node, &$form) {
  if ('advgallery' == $node->type) {
    if (is_array($node->field_advgallery) && count($node->field_advgallery) ) {
      if (count($node->field_advgallery) > ADVIMAGE_GALLERY_MAX_IMAGES_IN_GALLERY) {
        form_set_error('', t('Gallery contains more images then allowed. If error repeats while saving go to "Edit gallery" tab, try decrease number of images in gallery.') );
      }
      foreach ($node->field_advgallery as $img) {
        if (is_numeric($img['nid'] ) && isset($img['_error_element'] ) ) {
          $passed = FALSE;
          $result = db_query(
            'SELECT n.nid, n.uid FROM {node} n WHERE n.type=\'advimage\' AND n.nid=%d', 
            $img['nid']
          );
          if ($n = db_fetch_array($result) ) {
            $passed = ($n['uid'] == $node->uid);
          }
          if(!$passed) {
            form_set_error($img['_error_element'], t('Cannot add image to gallery when owners diferrs. If error repeats while saving go to "Edit gallery" tab and click save button there.') );
            break;
          }
        }
      }
    }
  }
}
/**
 * Implementation of hook_access().
 */
function advimage_access($op, $node, $account = NULL) {
  global $user;
  if ('advimage' == $node->type) {
    switch($op){
      case 'create':
        return user_access('create advimage content', $account);
      case 'delete':
        return (
          (user_access('delete own advimage content', $account)  && ($node->uid == $account->uid) )
          || user_access('delete any advimage content')
        );
      case 'edit':
        return (
          (user_access('edit own advimage content', $account)  && ($node->uid == $account->uid) )
          || user_access('edit any advimage content', $account)
        );
      case 'view':
        return NULL;
      default:
    }
  } elseif ('advgallery' == $node->type) {
    switch($op){
      case 'create':
        return user_access('create advgallery content', $account);
        break;
      case 'delete':
        return (
          (user_access('delete own advgallery content', $account)  && ($node->uid == $account->uid) )
          || user_access('delete any advgallery content')
        );
        break;
      case 'edit':
        return (
          (user_access('edit own advgallery content', $account)  && ($node->uid == $account->uid) )
          || user_access('edit any advgallery content', $account)
        );
      default:
    }
  }
}
/**
 * Implementation of hook_load().
 *
 * Now that we've defined how to manage the node data in the database, we
 * need to tell Drupal how to get the node back out. This hook is called
 * every time a node is loaded, and allows us to do some loading of our own.
 */
function advimage_load($node) {
  $additions = new stdClass();
  if ('advimage' == $node->type){
    // adding list of nodes referenced to us
    $result = db_query(
      'SELECT n.nid, n.title, n.uid FROM {node} n INNER JOIN {content_field_advgallery} c ON n.nid=c.nid AND n.vid=c.vid WHERE c.field_advgallery_nid=%d', $node->nid
    );
    $advgallery_list = array();
    while($o =  db_fetch_object($result) ) $advgallery_list []= $o;
    $additions->advgallery_list = $advgallery_list;
    return $additions;
  }
  if('advgallery' == $node->type) {
  }
  return $additions;
}
/**
 * Implementation of hook_view().
 */
function advimage_view($node, $teaser = FALSE, $page = FALSE) {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $node = node_prepare($node, $teaser);
  if ('advimage' == $node->type) {
    $res =  theme('advimage_advimage_where_in_galleries', $node);
    if (!empty($res) ) {
      $node->content['advgallery_list'] = array(
        '#value' => $res,
        '#weight' => 60,
      );
    }
  } elseif ('advgallery' == $node->type) {
    // readmore depends on how will be themed gallery
    // so im gallery view aspect that must know default site theme
    if (theme('advimage_view_advgallery_own_readmore_value', $node) ) {
      $node->readmore =  TRUE;
    }
  }
  return $node;
}
/**
 * Implementation of hook_theme
 */
function advimage_theme() {
  return array(
    'advimage_advimage_where_in_galleries' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node'),
    ),
    'advimage_gallery_view_mini_image' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('gnode', 'inode', 'image'),
    ),
    'advimage_gallery_view_mini_list' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('gnode', 'inode', 'images'),
    ),
    'advimage_gallery_view' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('gnode', 'inode'),
    ),
    'advimage_formatter_advimage_advgallery_teaser' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('element'),
    ),
    'advimage_formatter_advimage_advgallery_full' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('element'),
    ),
    'advimage_advgallery_empty_owner' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node', 'teaser'),
    ),
    'advimage_advgallery_empty_user' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node', 'teaser'),
    ),
    'advimage_gallery_list_teaser' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node'),
    ),
    'advimage_image_list_teaser' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node'),
    ),
    'advimage_my_galleries_list_nothing_found' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array(),
    ),
    'advimage_all_galleries_list_nothing_found' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array(),
    ),
    'advimage_my_images_list_nothing_found' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array(),
    ),
    'advimage_all_images_list_nothing_found' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array(),
    ),
    'advimage_user_galleries_nothing_found' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('account'),
    ),
    'advimage_user_all_images_nothing_found' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('account'),
    ),
    'advimage_view_advgallery_image_thumbnail' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node', 'image', 'teaser'),
    ),
    'advimage_view_advgallery_image_thumbnail_text' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node', 'image', 'teaser'),
    ),
    'advimage_view_advgallery_own_readmore_value' => array(
      'file' => 'advimage.theme.inc',
      'arguments' => array('node'),
    ),
    'advimage_view_advgallery_thumbnails' => array(
      'file' => 'advimage.theme.ing',
      'arguments' => array('node', 'images', 'teaser'),
    ),
    'advimage_edit_gallery_image_preview' => array(
      'file' => 'advimage.edit.inc',
      'arguments' => array('nstruct'),
    ),
    'advimage_edit_gallery_image_controls' => array(
      'file' => 'advimage.edit.inc',
      'arguments' => array('nstruct', 'ingallery'),
    ),
  );
}
/*
 * Implementation of CCK hook_field_formatter_info()
 */
function advimage_field_formatter_info(){
  return array(
    'advimage_advgallery_teaser' => array(
      'label' => t('Advimage teaser: Show image gallery teaser'),
      'field types' => array('nodereference'),
      'multiple values' =>  CONTENT_HANDLE_MODULE,
    ),
    'advimage_advgallery_full' => array(
      'label' => t('Advimage full: Show image gallery instead of all'),
      'field types' => array('nodereference'),
      'multiple values' =>  CONTENT_HANDLE_MODULE,
    ),
  );
}
/*
 * Access handlers for advimage_menu() handlers
 * advimage/mygalleries + advimage/allgalleries
 */
function advimage_galleries_list_access($which){
  switch($which) {
    case 'my':
      return user_access('create advgallery content') || user_access('access content');
      break;
    case 'foreign':
      return advimage_galleries_list_access('my') && user_access('view all users images listing');
      break;
  }
  return FALSE;
}
/*
 * access handler for: advimage/myimages advimage/allimages
 */
function advimage_images_list_access($which){
  switch ($which) {
    case 'my':
      return user_access('create advgallery content') || user_access('access content');
      break;
    case 'foreign':
      return advimage_galleries_list_access('my') && user_access('view all users images listing');
      break;
  }
  return FALSE;
}
/*
 * access  hadler for node/%node/advimage_editgallery
 * 
 */
function advimage_edit_gallery_access($node) {
  global $user;
  if('advgallery' != $node->type) return FALSE;
  if ($node->uid == $user->uid){
    return user_access('edit own advgallery content') || user_access('edit any advgallery content');
  } else {
    return user_access('edit any advgallery content');
  }
  return FALSE;
}
/*
 * access  hadler for user/%user/advimage_editgallery
 * 
 */
function advimage_user_galleries_access($account) {
  global $user;
  if ($account->uid == $user->uid) {
    return TRUE;
  } else {
    return user_access('view foreign galleries list in user profile');
  }
  return FALSE;
}
/*
 * access  hadler for user/%user/advimage/allimages
 * 
 */
function advimage_user_all_images_access($account) {
  global $user;
  if ($account->uid == $user->uid) {
    return TRUE;
  } else {
    return user_access('view foreign images pool list in user profile');
  }
  return FALSE;
}
/*
 * access handler for node/%node/advgallery/%node
 */
function advimage_gallery_view_access($node1, $node2) {
  if (node_access('view', $node1) ) {
    if(
      isset ($node1->field_advgallery) 
      && is_array($node1->field_advgallery) 
      && count($node1->field_advgallery) 
    ) {
      foreach($node1->field_advgallery as $value) {
        if($value['nid'] == $node2->nid) return node_access('view', $node2);
      }
    }
  }
  return FALSE;
}
