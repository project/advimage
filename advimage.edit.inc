<?php
/*
 * @file
 * All about gallery editing. Menu handler, additional functions
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
/*
 * Menu handler for advimage edit gallery page
 */
function advimage_edit_gallery($node) {
  drupal_set_title(check_plain($node->title) );
  return '<div id="advimage-edit-gallery-form-nojavascript-warning">'
    . t('Warning: if Javascript is not enabled in your browser - you will not be able edit gallery content using this page. Sorry for inconveniences.')
    . '</div>' 
    . drupal_get_form('advimage_edit_gallery_form', $node);
}
/*
 * Menu handler for advimage edit gallery page
 * This is Json handler intended to find image by it's node url
 */
function advimage_edit_gallery_getimagebyurl_json($node) {
  global $user, $base_path, $base_url;
  drupal_set_header('Content-type: text/javascript; charset=utf-8');
  drupal_set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  drupal_set_header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header('Cache-Control: no-store, no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');
  $token_name = arg(4);
  $token_value = arg(5);
  if ( !_advimage_edit_gallery_check_token($node, $token_name, $token_value) ) {
    $result = array(
      'jsonrpc' => '2.0', 'result' => 'error',
      'error_token' => t('Looks like gallery was saved or changed in the another form. Please reload this page and try edit gallery again.'),
    );
    die(drupal_to_js($result) );
  }
  if (!isset($_POST['addbyurlimage'] ) ) {
    die(drupal_to_js(array(
      'jsonrpc' => '2.0', 
      'result' => 'error', 
      'message' => t('Looks like wrong form usage error. If it repeats again tell to your site administrator')
    ) ) );
  }
  $addbyurlimage = '' . $_POST['addbyurlimage'];
  if (strpos($addbyurlimage, $base_url) === 0) {
    $addbyurlimage = substr($addbyurlimage, strlen($base_url) );
  }
  if (strpos($addbyurlimage, '/') === 0) {
    $addbyurlimage = substr($addbyurlimage, 1);
  }
  $addbyurlimage = drupal_get_normal_path($addbyurlimage);
  $matches = array();
  if (preg_match('#node/(\d+)#', $addbyurlimage, $matches) ) {
    $result =  db_query(
      db_rewrite_sql(
      'SELECT n.nid, n.created, n.vid, n.title, 0 AS delta, f.fid, f.filename, 
  f.filepath, f.filemime, f.filesize
FROM {node} n INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE n.nid=%d AND n.uid=%d AND n.status=1'
      ),
      $matches[1], $user->uid
    );
    if ($nstruct = db_fetch_array($result) ) {
      if (
        is_array($node->field_advgallery) 
        && (count($node->field_advgallery) == ADVIMAGE_GALLERY_MAX_IMAGES_IN_GALLERY)
      ) {
        die(drupal_to_js(array(
          'jsonrpc' => '2.0', 
          'result' => 'error', 
          'message' => t('Gallery has reached maximim possible amount of images')
        ) ) );
      }
      die(drupal_to_js(array(
        'jsonrpc' => '2.0', 
        'result' => 'success',
        'nid' => $nstruct['nid'],
        'html' => _advimage_edit_gallery_html_image_item($nstruct, TRUE),
      ) ) );
    } else {
      die(drupal_to_js(array(
        'jsonrpc' => '2.0', 
        'result' => 'error', 
        'message' => t('Cann not find ready published image which belongs to your account by this url')
      ) ) );
    }
  } else {
    die(drupal_to_js(array(
      'jsonrpc' => '2.0', 
      'result' => 'error', 
      'message' => t('You try using wrong url to image node')
    ) ) );
  }
}
/*
 * Menu handler for advimage edit gallery page
 * This is Json handler intended to save gallery by pressing save button
 */
function advimage_edit_gallery_savegallery_json($node) {
  drupal_set_header('Content-type: text/javascript; charset=utf-8');
  drupal_set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  drupal_set_header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header('Cache-Control: no-store, no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');
  $token_name = arg(4);
  $token_value = arg(5);
  if ( !_advimage_edit_gallery_check_token($node, $token_name, $token_value) ) {
    $result = array(
      'jsonrpc' => '2.0', 'result' => 'error',
      'error_token' => t('Looks like gallery was saved or changed in the another form. Please reload this page and try edit gallery again.'),
    );
    die(drupal_to_js($result) );
  }
  if (!isset($_POST['nodes'] ) ) {
    die(drupal_to_js(array(
      'jsonrpc' => '2.0', 
      'result' => 'error', 
      'message' => t('Looks like wrong form usage error. If it repeats again tell to your site administrator')
    ) ) );
  }
  $nodes = explode(' ', $_POST['nodes'] );
  foreach ($nodes as $key => $val) {
    $nodes[$key] = explode(':', $val);
    if (count($nodes[$key]) == 2) {
      $nodes[$key] = @intval($nodes[$key][1] );
    } else {
      unset($nodes[$key] );
    }
  }
  $nodes2 = array();
  if (count($nodes) ) foreach($nodes as $nid) $nodes2 []= $nid;
  $nodes = $nodes2;
  unset($nodes2);
  // trololo protection
  if (count($nodes) > ADVIMAGE_GALLERY_MAX_IMAGES_IN_GALLERY) {
    $result = array(
      'jsonrpc' => '2.0', 
      'result' => 'error',
      'error_token' => t(
        'You try add more than !count images to gallery', 
        array('!count' => ADVIMAGE_GALLERY_MAX_IMAGES_IN_GALLERY) 
      ),
    );
    die(drupal_to_js($result) );
  }
  $n1 =  array();
  // make more than one request - cause of line may be too long
  $node_count =  count($nodes);
  $npq = 15;
  $cnum = floor($node_count / $npq) + ( ( ($node_count % $npq) > 0) ? 1 : 0);
  for ($i = 0; $i < $cnum; $i ++) {
    $nf =  array();
    $j1 = $i * $npq;
    $j2 = ($i + 1) * $npq;
    if ($j2 > $node_count) {
      $j2 = $node_count;
    }
    for ($j = $j1; $j < $j2; $j++) {
      $nf []= $nodes[$j];
    }
    $result =  db_query(
      'SELECT n.nid FROM {node} n WHERE n.type=\'advimage\' AND n.status=1 AND n.nid IN(%s) AND n.uid=%d', 
      implode(',', $nf),
      $node->uid
    );
    unset($nf);
    while($row =  db_fetch_array($result) ) {
      $n1[$row['nid'] ] =  $row['nid'];
    }
  }
  foreach ($nodes as $key => $value) {
    if (isset($n1[$value] ) ) {
      $nodes[$key] = array('nid' => $value);
    } else {
      unset($nodes[$key] );
    }
  }
  unset($n1);
  $node->field_advgallery = $nodes;
  node_save($node);
  die(drupal_to_js( array(
    'jsonrpc' => '2.0',
    'result' => 'success',
  ) ) );
}
/*
 * Menu handler for advimage edit gallery page
 * Additional JSON handler to return more free non used in te gallery user images
 */
function advimage_edit_gallery_morefreeimages_json($node) {
  drupal_set_header('Content-type: text/javascript; charset=utf-8');
  drupal_set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  drupal_set_header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header('Cache-Control: no-store, no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');
  $page =  intval($_GET['page'] );
  $token_name = arg(4);
  $token_value = arg(5);
  if ( !_advimage_edit_gallery_check_token($node, $token_name, $token_value) ) {
    $result = array(
      'jsonrpc' => '2.0', 'result' => 'error',
      'error_token' => t('Looks like gallery was saved or changed in the another form. Please reload this page and try edit gallery again.'),
    );
    die(drupal_to_js($result) );
  } 
  // now all ok - getting data
  $total = _advimage_edit_get_free_images_count($node->nid);
  $moreavailable = $total > ($page + 1) * ADVIMAGE_EDIT_PAGE_NUM;
  $imageshtml = _advimage_edit_get_free_images_html($node, $page);
  die(drupal_to_js( array(
    'jsonrpc' => '2.0',
    'result' => 'success',
    'imageswerereturned' => !empty($imageshtml),
    'moreavailable' => $moreavailable,
    'html' => $imageshtml,
  ) ) );
}

/*
 * Advimage edit gallery form - reorder/add/remove images from the gallery
 */
function advimage_edit_gallery_form(&$form_state, $node) {
  global $user;
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  jquery_ui_add(
    array('ui.sortable')
  );
  drupal_add_js($mpath .'/js/advimage_edit.js', 'module', 'footer');
  $form = array();
  $result =  db_query(
    'SELECT 
   DISTINCT(n.nid) AS nid, 
   n.created, n.vid, n.title, ag.delta, f.fid, f.filename, f.filepath, f.filemime, f.filesize
FROM {content_field_advgallery} ag
INNER JOIN {node} n ON ag.field_advgallery_nid=n.nid
INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE ag.nid=%d AND ag.vid=%d AND n.uid=%d ORDER BY ag.delta, n.created DESC',
    $node->nid, $node->vid, $node->uid
  );
  $images = array();
  while($row = db_fetch_array($result) ) $images []= $row;
  $form['advgalleryimages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Images in gallery'),
    '#description' => t('Move images up and down to reorder, or click there to remove images from gallery'),
    'savepoint' => array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="advgallery-savepoint">',
      '#suffix' => '</div>',
      '#description' => t('To save gallery you must click save button below the form'),
    ),
    'ingalleryimages' => array(
      '#type' => 'markup',
      '#prefix' => '<div id="advimage-edit-gallery-ingallery">',
      '#suffix' => '</div>',
      '#value' => '',
    ),
  );
  if (count($images) ) {
    foreach ($images as $image) {
      $form['advgalleryimages']['ingalleryimages']['#value'] .= _advimage_edit_gallery_html_image_item($image, TRUE);
    }
  } else {
    $form['advgalleryimages']['ingalleryimages']['#value'] .= '<div class="noimages">'
      . t('There are no images in the gallery but you can select and add them below.')
      . '</div>';
  }
  $onsiteimages = _advimage_edit_get_free_images_html($node, 0);
  if ( empty($onsiteimages) ) {
    $onsiteimages = '<div class="noimages">' . t('There are no images to add to the gallery') . '</div>';
  }
  $form['onsiteimages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available images to add to the gallery'),
    '#description' => t('Here you can see images to add to the gallery. Select  images you need by clicking mouse and press add button to add images to the galery'),
    'otherimages' => array(
      '#type' => 'markup',
      '#prefix' => '<div id="advimage-edit-gallery-otherimages">',
      '#suffix' => '</div>',
      '#value' => '<div id="advimage-edit-gallery-otherimages-int">' . $onsiteimages . '</div>',
      'moreonsiteimages' => array(
        '#type' => 'button',
        '#value' => t('Show more images'),
        '#prefix' => '<div id="advimage-edit-gallery-otherimages-more">',
        '#suffix' => '</div>',
      ),
    ),
  );
  $form['addbyurl'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add image by it\'s url to the gallery'),
    '#description' => t('Sometimes it\'s too difficult to find image in listing, that\'s why here you can enter URL of image to add it to this gallery. Image must belong to your user account and may be from other image gallery. This oprion was made special for such exception cases'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'addbyurlimage' => array(
      '#type' => 'textfield',
      '#title' => t('Url of image'),
      '#description' => t('Please enter image url here. And than click Add button'),
      '#value' => '',
    ),
    'addbyurlbutton' => array(
      '#type' => 'button',
      '#value' => 'Add image',
      '#prefix' => '<div id="addbyurlbutton-div" class="form-item"><br />',
      '#suffix' => '</div>',
    ),
  );
  // now render own form session to check form is correct + node nid + additional values
  // this values will be operable by JS
  $tokenpair = _advimage_edit_gallery_generate_token_pair($node);
  $form['advimage_edit_token_name'] = array('#type' => 'hidden', '#value' => $tokenpair['name'] );
  $form['advimage_edit_token_token'] = array('#type' => 'hidden', '#value' => $tokenpair['token'] );
  $form['advimage_edit_node_nid'] = array('#type' => 'hidden', '#value' => $node->nid);
  $form['savebutton'] = array(
    '#type' => 'submit',
    '#value' => t('Save gallery'),
    '#prefix' => '<div id="advimage-edit-gallery-savebutton">',
    '#suffix' => '</div>',
  );
  // various JS settings we need
  $settings = array(
    'advimage' => array(
      'showmoreimages_json_url' => url(
        'node/' . $node->nid . '/advimage_editgallery/getmorefreeimages/' 
          . $tokenpair['name'] . '/' . $tokenpair['token']
      ),
      'savegallery_json_url' => url(
        'node/' . $node->nid . '/advimage_editgallery/savegallery/' 
          . $tokenpair['name'] . '/' . $tokenpair['token']
      ),
      'getimagebyurl_json_url' => url(
        'node/' . $node->nid . '/advimage_editgallery/getimagebyurl/' 
          . $tokenpair['name'] . '/' . $tokenpair['token']
      ),
      'gallery_node_url' => url('node/' . $node->nid),
      'advimage_edit_page_num' => ADVIMAGE_EDIT_PAGE_NUM,
      'gallery_max_images' => ADVIMAGE_GALLERY_MAX_IMAGES_IN_GALLERY,
    )
  );
  // here we send some settings to JS we want to use :)
  drupal_add_js($settings, 'setting');
  return $form;
}

/*
 * Result - an array('name' => 'token unique value', 'token' => 'token checksum')
 * With static storage - to not regenerate twice
 * depends both on node and $node->changed
 */
function _advimage_edit_gallery_generate_token_pair($node) {
  static $pairs;
  if ( isset($pairs) ) { 
    $pairs =  array();
  }
  if (isset($pairs[$node->nid] ) ) {
    return $pairs[$node->nid];
  }
  $pairs[$node->nid]['name'] = 'advimage_edit_gallery_' . $node->nid . '_' . str_replace('.', '_', uniqid('', TRUE));
  $pairs[$node->nid]['token'] = drupal_get_token($pairs[$node->nid]['name'] . '_' . $node->changed);
  return $pairs[$node->nid];
}
function _advimage_edit_gallery_check_token($node, $name, $token) { 
  return drupal_valid_token($token, $name . '_' . $node->changed);
}
function _advimage_edit_get_free_images_html($node, $page = 0, $page_amount = ADVIMAGE_EDIT_PAGE_NUM) {
  $out = '';
  $images = _advimage_edit_get_free_images_array($node, $page, $page_amount);
  if (count($images) ) {
    foreach ($images as $image) {
      $out .=  _advimage_edit_gallery_html_image_item($image, FALSE);
    }
  }
  return $out;
}

function _advimage_edit_get_free_images_count($for_parent_nid = NULL) {
  global $user;
  if ($for_parent_nid) {
    $result = db_query(
      'SELECT COUNT(DISTINCT(n.nid)) AS c FROM {node} n INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
LEFT JOIN {content_field_advgallery} ag ON n.nid=ag.field_advgallery_nid
LEFT JOIN {node} n1 ON n1.vid=ag.vid and n1.nid=ag.nid
WHERE n.uid=%d AND n.type=\'advimage\' AND (NOT(n1.nid=%d) OR n1.nid IS NULL)
ORDER BY n.created DESC',
      $user->uid,
      $for_parent_nid,
      $page * $page_amount, $page_amount
    );
  } else {
    // alternate variant - for all
    $result = db_query_range(
      db_rewrite_sql(
        'SELECT  COUNT(DISTINCT(n.nid)) AS c FROM {node} n INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
LEFT JOIN {content_field_advgallery} ag ON n.nid=ag.field_advgallery_nid
LEFT JOIN {node} n1 ON n1.vid=ag.vid and n1.nid=ag.nid
WHERE n.uid=%d AND n.type=\'advimage\' AND (n1.nid IS NULL)
ORDER BY n.created DESC'
      ),
      $user->uid,
      $page * $page_amount, $page_amount
    );
  }
  while($row = db_fetch_array($result) ) return $row['c'];
  return 0;
}
/*
 * gets some onsite images of current user starting from the given node created time
 */
function _advimage_edit_get_free_images_array(
  $node, $page = 0, $page_amount = ADVIMAGE_EDIT_PAGE_NUM
) {
  global $user;
  $result = db_query_range(
    'SELECT 
DISTINCT(n.nid) AS nid, n.created, n.vid, n.title, ag.delta AS delta, f.fid, f.filename, f.filepath, f.filemime, f.filesize
FROM {node} n INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
LEFT JOIN {content_field_advgallery} ag ON n.nid=ag.field_advgallery_nid
LEFT JOIN {node} n1 ON n1.vid=ag.vid and n1.nid=ag.nid
WHERE n.uid=%d AND n.type=\'advimage\' AND (n1.nid IS NULL)
ORDER BY n.created DESC',
    $node->uid,
    $page * $page_amount, $page_amount
  );
  $images = array();
  while($row = db_fetch_array($result) ) $images []= $row;
  return $images;
}
/*
 * Display html div with hidden data inside to show draggable, 
 * sortable html item with gallery image and add/delete controls 
 */
function _advimage_edit_gallery_html_image_item($nstruct, $ingallery = TRUE) {
  $out = '<div class="advimage-edit-gallery-image" id="advimage-edit-gallery-image-' . $nstruct['nid'] . '">';
  $out .= theme('advimage_edit_gallery_image_preview', $nstruct);
  $out .= theme('advimage_edit_gallery_image_controls', $nstruct, $ingallery);
  $out .= '</div>';
  return $out;
}
/*
 * Displays advimage node preview in the edit page
 */
function theme_advimage_edit_gallery_image_preview($nstruct) {
  $iurl =  imagecache_create_url('advimage_preview1', $nstruct['filepath'] );
  return '<div class="advimage-edit-gallery-image-preview" id="advimage-edit-gallery-image-preview-' . $nstruct['nid'] . '">'
    . '<div class="advimage-edit-gallery-image-preview-image">' . l(
      '<img src="' . $iurl . '"  alt="' . check_plain($nstruct['title'] ) 
        . '" title="' . check_plain($nstruct['title'] ) . '" />', 
      'node/' .$nstruct['nid'], array('html' => TRUE, 'attributes' => array('rel' => $nstruct['nid'] ) ) 
    ) . '</div>'
    . '<h3>' . l($nstruct['title'], 'node/' .$nstruct['nid'], array( 'attributes' => array('rel' => $nstruct['nid'] ) )  ) . '</h3></div>';
}
/*
 * Displays controls on what to do with item - add or delete from gallery, etc.
 */
function theme_advimage_edit_gallery_image_controls($nstruct, $ingallery) {
  $out = '<div class="advimage-edit-gallery-image-controls">';
  $out .= '<ul class="advimage-edit-gallery-image-controls-list">';
  if ($ingallery)  {
    $out .= '<li><a href="#" class="advimage-edit-gallery-remove-image" rel="' . $nstruct['nid'] . '">'
      . t('Remove image from gallery') . '</a></li>';
  } else {
    $out .= '<li><a href="#" class="advimage-edit-gallery-add-image" rel="' . $nstruct['nid'] . '">'
      . t('Add image to gallery') . '</a></li>';
  }
  $out .= '</ul>';
  $out .= '</div>';
  return $out;
}
