<?php
// $Id$
/**
 * @file
 * Main menu handlers for displaing galleries, etc
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */

/*
 * Function to show single gallery images  for the advimage module
 */
function advimage_gallery_view($node1, $node2) {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  drupal_add_js($mpath .'/js/advimage_view.js', 'module', 'footer');
  drupal_set_title(t(
    'Gallery: "!title1", Image: "!title2"',
    array('!title1' => $node1->title, '!title2' => $node2->title)
  ) );
  $breadcrumb =  array(l(t('Home'), '<front>'), l($node1->title, 'node/' . $node1->nid) );
  drupal_set_breadcrumb($breadcrumb);
  return theme('advimage_gallery_view', $node1, $node2);
}
function advimage_my_galleries_list() {
  global $user;
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $result = pager_query(
    db_rewrite_sql(
      'SELECT DISTINCT n.nid FROM {node} n 
INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid WHERE n.uid=%d AND n.type=\'advgallery\'
ORDER BY n.sticky DESC, n.created DESC'
    ),
    variable_get('default_nodes_main', 10), 0, NULL, $user->uid
  );
  $nids = array();
  $nodes = array();
  while ($o = db_fetch_object($result) ) {
    $nids []= $o->nid;
    if ($node = node_load(array('nid' => $o->nid) ) ) {
      $nodes []= $node;
    }
  }
  if ( count($nodes) ) {
    $out = '';
    foreach ($nodes as $node) $out .= theme('advimage_gallery_list_teaser', $node);
    $out .= theme('pager');
    return $out;
  } else {
    return theme('advimage_my_galleries_list_nothing_found');
  }
  return '';
}
function advimage_all_galleries_list() {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $result = pager_query(
    db_rewrite_sql(
      'SELECT DISTINCT n.nid FROM {node} n 
INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid WHERE n.type=\'advgallery\'
ORDER BY n.sticky DESC, n.created DESC'
    ),
    variable_get('default_nodes_main', 10), 0, NULL
  );
  $nids = array();
  $nodes = array();
  while ($o = db_fetch_object($result) ) {
    $nids []= $o->nid;
    if ($node = node_load(array('nid' => $o->nid) ) ) {
      $nodes []= $node;
    }
  }
  if ( count($nodes) ) {
    $out = '';
    foreach ($nodes as $node) $out .= theme('advimage_gallery_list_teaser', $node);
    $out .= theme('pager');
    return $out;
  } else {
    return theme('advimage_all_galleries_list_nothing_found');
  }
  return '';
}
function advimage_my_images_list() {
  global $user;
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $result = pager_query(
    db_rewrite_sql(
      'SELECT DISTINCT n.nid FROM {node} n 
INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid WHERE n.uid=%d AND n.type=\'advimage\'
ORDER BY n.sticky DESC, n.created DESC'
    ),
    variable_get('default_nodes_main', 10), 0, NULL, $user->uid
  );
  $nids = array();
  $nodes = array();
  while ($o = db_fetch_object($result) ) {
    $nids []= $o->nid;
    if ($node = node_load(array('nid' => $o->nid) ) ) {
      $nodes []= $node;
    }
  }
  if ( count($nodes) ) {
    $out = '';
    foreach ($nodes as $node) $out .= theme('advimage_image_list_teaser', $node);
    $out .= theme('pager');
    return $out;
  } else {
    return theme('advimage_my_images_list_nothing_found');
  }
  return '';
}
function advimage_all_images_list() {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $result = pager_query(
    db_rewrite_sql(
      'SELECT DISTINCT n.nid FROM {node} n 
INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid WHERE n.type=\'advimage\'
ORDER BY n.sticky DESC, n.created DESC'
    ),
    variable_get('default_nodes_main', 10), 0, NULL
  );
  $nids = array();
  $nodes = array();
  while ($o = db_fetch_object($result) ) {
    $nids []= $o->nid;
    if ($node = node_load(array('nid' => $o->nid) ) ) {
      $nodes []= $node;
    }
  }
  if ( count($nodes) ) {
    $out = '';
    foreach ($nodes as $node) $out .= theme('advimage_image_list_teaser', $node);
    $out .= theme('pager');
    return $out;
  } else {
    return theme('advimage_all_images_list_nothing_found');
  }
  return '';
}
function advimage_user_galleries($account) {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $result = pager_query(
    db_rewrite_sql(
      'SELECT DISTINCT n.nid FROM {node} n 
INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid WHERE n.uid=%d AND n.type=\'advgallery\'
ORDER BY n.sticky DESC, n.created DESC'
    ),
    variable_get('default_nodes_main', 10), 0, NULL, $account->uid
  );
  $nids = array();
  $nodes = array();
  while ($o = db_fetch_object($result) ) {
    $nids []= $o->nid;
    if ($node = node_load(array('nid' => $o->nid) ) ) {
      $nodes []= $node;
    }
  }
  if ( count($nodes) ) {
    $out = '';
    foreach ($nodes as $node) $out .= theme('advimage_gallery_list_teaser', $node);
    $out .= theme('pager');
    return $out;
  } else {
    return theme('advimage_user_galleries_nothing_found', $account);
  }
}
function advimage_user_all_images($account) {
  $mpath = drupal_get_path('module', 'advimage');
  drupal_add_css($mpath .'/css/advimage.css', 'module');
  $result = pager_query(
    db_rewrite_sql(
      'SELECT DISTINCT n.nid FROM {node} n 
INNER JOIN {node_revisions} r ON n.nid=r.nid AND n.vid=r.vid WHERE n.uid=%d AND n.type=\'advimage\'
ORDER BY n.sticky DESC, n.created DESC'
    ),
    variable_get('default_nodes_main', 10), 0, NULL, $account->uid
  );
  $nids = array();
  $nodes = array();
  while ($o = db_fetch_object($result) ) {
    $nids []= $o->nid;
    if ($node = node_load(array('nid' => $o->nid) ) ) {
      $nodes []= $node;
    }
  }
  if ( count($nodes) ) {
    $out = '';
    foreach ($nodes as $node) $out .= theme('advimage_image_list_teaser', $node);
    $out .= theme('pager');
    return $out;
  } else {
    return theme('advimage_user_all_images_nothing_found', $account);
  }
  return '';
}

