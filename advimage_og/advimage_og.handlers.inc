<?php
/*
 * @file 
 * Module: advimage_og
 * Advanced images opengroups integration page
 * Standard menu handlers functions
 * (c) Ilya V. Azarov, brainstorm@brainstorm.name, 2011
 */
 
/*
 * This hander displays edit tab with listing of images and ability select own og's for each or
 * set/add permissions from the parent gallery
 */
function advimage_og_edit_gallery($node) {
  // see advimage_edit_gallery_form() to know how and what the fuck
  $result =  db_query(
    'SELECT 
   DISTINCT(n.nid) AS nid,
   /* added - differs from parent - we need a type here to emulate node behaviour */
   n.type, 
   n.created, n.vid, n.title, ag.delta, f.fid, f.filename, f.filepath, f.filemime, f.filesize
FROM {content_field_advgallery} ag
INNER JOIN {node} n ON ag.field_advgallery_nid=n.nid
INNER JOIN {content_type_advimage} ai ON n.nid=ai.nid AND n.vid=ai.vid
INNER JOIN {files} f ON f.fid=ai.field_advimage_fid
WHERE ag.nid=%d AND ag.vid=%d AND n.uid=%d ORDER BY ag.delta, n.created DESC',
    $node->nid, $node->vid, $node->uid
  );
  $inodes = array();
  // here they will be objects cause we need some nodeapi manipulations
	while ($o = db_fetch_object($result) ) {
		$inodes []= $o;
	}
	// this will be heavy load but there is no stright way with og
	foreach ($inodes as $key => $inode) {
		// call hook_nodeapi() from og module
		og_nodeapi($inode, 'load');
		og_access_nodeapi($inode, 'load');	
	}
	$out = '<div id="advimage-og-edit-gallery-no-js-warning">' . t('If form is not displayed - that means JavaScript is not enabled or not supported by your browser') . '</div>';
	$out .= '<div id="advimage-og-edit-gallery-form-messages"></div>';
	$out .= '<div id="advimage-og-edit-gallery">';
	$out .= '<div id="main-gallery-settings-form">';
	$out .= '<h3 class="title">' .  t('Main gallery settings') .'</h3>';
	$out .= '<div id="advgallery-savepoint"><fieldset><div class="description">' 
		. t('To save gallery you must click save button below the form') .' </div></fieldset></div>';
	$out .= '<div class="advimage-og-prepare-node-fieldset">' . advimage_og_prepare_node_fieldset($node)
		. '</div>';
	$out .= '<div class="advimage-og-distribute-rights-buttons">';
	$out .= '<input class="form-submit" type="button" id="edit-distribute-setsamerights" name="distribute-samerights" value="' . t('Set same gallery groups to images') . '" />';
	$out .= '<input class="form-submit" type="button" id="edit-distribute-mergerights" name="distribute-mergerights" value="' . t('Only add gallery groups to images') . '" />';
	$out .= '<input class="form-submit" type="button" id="edit-distribute-makeallpublic" name="distribute-makeallpublic" value="' . t('Make all images public') . '" />';
	$out .= '<input class="form-submit" type="button" id="edit-distribute-makeprivate" name="distribute-makeallpublic" value="' . t('Make all images private group content') . '" />';
	$out .= '</div>'; //class="advimage-og-distribute-rights-buttons"
	$out .= '</div>'; //id="main-gallery-settings-form"
	$out .= '<h2 class="title">' . t('Set groups and access rights to images') . '</h2>';
	$out .= '<div id="advimage-og-edit-gallery-ingallery">';
	foreach($inodes as $inode) {
		$out .= '<div id="advimage-og-edit-gallery-image-' . $inode->nid . '" class="advimage-og-edit-gallery-image" rel="' . $inode->nid . '">';
		// what the fuckin image is itself
		$out .= theme('advimage_edit_gallery_image_preview', (array) $inode);
		// rights settings form
		$out .= '<div id="advimage-og-edit-gallery-image-fieldset' . $inode->nid . '" class="advimage-og-edit-gallery-image-fieldset">';
		$out .= advimage_og_prepare_node_fieldset($inode);
		$out .= '</div>'; //id="advimage-og-edit-gallery-image-fieldset' . $inode->nid . '
		$out .= '</div>'; //id="advimage-og-edit-gallery-image-' . $inode->nid . '
	}
	$out .= '</div>';//id="advimage-og-edit-gallery-ingallery"
	$out .= '<div id="advimage-og-edit-gallery-save-block"><input class="form-submit" type="button" id="edit-save" name="edit-save" value="' . t('Save values') . '" /></div>';
	$out .= '</div>'; //id="advimage-og-edit-gallery"
	$mpath = drupal_get_path('module', 'advimage_og');
  drupal_add_css($mpath .'/css/advimage_og.css', 'module');
  $token_name = 'advimage_og_edit_gallery_' . $node->nid . '_' . str_replace('.', '_', uniqid('', TRUE) );
  $token = drupal_get_token($token_name . '_' . $node->nid  . '_' . $node->changed);
  $settings = array(
		'advimage_og' => array(
			'advgallery_nid' => $node->nid,
			'advgallery_gallery_url' => url('node/' . $node->nid, array('absolute' => TRUE) ),
			'advgallery_save_url' => url(
				'node/' . $node->nid . '/advimage_og_access/gjson/' . $token_name . '/' . $token, 
				array('absolute' => TRUE) 
			),
			'advimage_batch_save_url' => url(
			  'node/' . $node->nid . '/advimage_og_access/ijson/' . $token_name . '/' . $token, 
				array('absolute' => TRUE)
			),
			'og_images_num_save_per_request' => ADVIMAGE_OG_IMAGES_NUM_SAVE_PER_REQUEST,
		),
  );
  drupal_add_js($settings, 'setting');
  drupal_add_js($mpath .'/js/advimage_og.js', 'module', 'footer');
	return $out;
}
function advimage_og_prepare_node_fieldset($node){
	$form = array();
	$form['#node'] = $node;
	_advimage_og_form_add_og_audience($form, $form_state);
	if (module_exists('og_access') ) {
		_advimage_og_access_alter_nongroup_form($form, $form_state, $node);
	}
	$form['og_nodeapi']['#collapsible'] = FALSE;
	if (isset($form['og_nodeapi']['visible'] ) ) {
		if (isset($form['og_nodeapi']['#title'] ) ) {
			$form['og_nodeapi']['#title'] = t('Select groups you want place node in');
		};
		foreach($form['og_nodeapi']['visible'] as $key => $value) {
			unset($form['og_nodeapi']['visible'][$key] );
			$form['og_nodeapi']['visible']['node_' . $key] = $value;
			$form['og_nodeapi']['visible']['node_' . $key]['#id'] = 'edit-node-' 
				. str_replace('_', '-', $key) . '-' . $node->nid;
		}
		drupal_prepare_form($type . '_node_form', $form, $form_state);
		drupal_process_form($type . '_node_form', $form, $form_state);
		$formrender =  drupal_render($form['og_nodeapi'] );
	}
	return $formrender;
}
// rewritten og_form_add_og_audience() hook  - I need select only look there
function _advimage_og_form_add_og_audience(&$form, &$form_state) {
  global $user;
  // Determine the selected groups if it is stored in the form_state.
  if (!empty($form_state['og_gids'][0]) && empty($form_state['og_groups'])) {
    $gids = explode(',', $form_state['og_gids'][0]);
  }

  $node = $form['#node'];
  $required = variable_get('og_audience_required', 0) && !user_access('administer nodes');
  $is_optgroup = FALSE;

  // Determine the list of groups that are shown.
  // Start by collecting all groups that the user is a member of.
  $subs = og_get_subscriptions($user->uid);
  $options = array();
  foreach ($subs as $key => $val) {
    $options[$key] = $val['title'];
  }
  if (user_access('administer nodes')) {
    // Node admins see all of groups.
    $all = og_all_groups_options();
    $other = array_diff_assoc($all, $options);
    // Use an optgroup if admin is not a member of all groups.
    if ($other) {
      $options = array(
        t('My groups') => $options,
        t('Other groups') => $other,
      );
      $is_optgroup = TRUE;
    }
    else {
      $options = $all;
    }
  }
  else {
    // Classify those groups which the node already has but the author does not.
    if (!isset($node->og_groups_both)) {
      $node->og_groups_both = array();
    }
    $current_groups = og_node_groups_distinguish($node->og_groups_both);
    // Put inaccessible groups in the $form so that they can persist. See og_presave_group() and og_access_alter_nongroup_form() in og_access.module
    $form['og_invisible']['og_groups_inaccessible'] = array('#type' => 'value', '#value' => $current_groups['inaccessible']);

    // Add the accessible groups that they node already belongs to.
    if ($current_groups['accessible']) {
      // Use an optgroup to distinguish between my memberships and additional groups in the Audience select.
      // There is code below which assumes that $options does not have optgroups but that code is within a $simple check
      // So we are OK as long as $simple does not apply to node edits.
      // NOTE: If you form_alter the audience element, beware that it can sometimes be an optgroup.
      foreach ($current_groups['accessible'] as $key => $val) {
        $other[$key] = $val['title'];
      }

      $options = array(
        t('My groups') => $options,
        t('Other groups') => $other,
      );
      $is_optgroup = TRUE;
    }
  }

  // Show read only item if we are non-admin, and in simple mode (i.e. non-checkboxes) and at least one group is in querystring
  $simple = !user_access('administer organic groups') && !variable_get('og_audience_checkboxes', TRUE) && count($gids);

  // determine value of audience multi-select
  if (count($options) == 1 && $required) {
    $gids = array_keys($options);
    $gid = $gids[0];
    $groups = array($gid);
    // also show read only mode if user has 1 option and we are in required mode
    $simple = TRUE;
  }
  elseif (!empty($gids)) {
   // populate field from the querystring if sent
   $groups = $gids;
   if (!user_access('administer nodes') && $simple) {
     // filter out any groups where author is not a member. we cannot rely on fapi to do this when in simple mode.
     $groups = array_intersect($gids, array_keys($options));
   }
  }
  elseif (isset($node->og_groups)) {
    $groups = $node->og_groups;
  }
  else {
    $groups = array();
  }
  // This is only used by og_access module right now.
  $form['og_initial_groups'] = array(
    '#type' => 'value',
    '#value' => $groups,
  );

  if (module_exists('content')) {
    $form['og_nodeapi']['#group'] = 'additional_settings';
    $form['og_nodeapi']['#weight'] = content_extra_field_weight($form['#node']->type, 'og_nodeapi');
  }

  // Emit the audience form element.
  if ($simple) {
    // 'simple' mode. read only.
    if (count($groups)) {
      foreach ($groups as $gid) {
        $titles[] = $options[$gid];
        $item_value = implode(', ', $titles);
      }
      $form['og_nodeapi']['visible']['og_groups_visible'] = array(
        '#type' => 'item',
        '#title' => t('Audience'),
        '#value' => $item_value
      );
      $assoc_groups = drupal_map_assoc($groups);

      // this 'value' element persists the audience value during submit process
      $form['og_nodeapi']['invisible']['og_groups'] = array('#type' => 'value', '#value' => $assoc_groups);
    }
  }
  elseif ($cnt = count($options, COUNT_RECURSIVE)) {
    // show multi-select. if less than 20 choices, use checkboxes.
    $type = 'select';
    $max_groups = variable_get('og_max_groups_'.$node->type,'');
    $description_max_groups = ($max_groups && !user_access('administer nodes')) ? format_plural($max_groups," Limited to !max_groups choice."," Limited to !max_groups choices.", array('!max_groups' => $max_groups)):'';
    $form['og_nodeapi']['visible']['og_groups'] = array(
      '#type' => $type,
      '#title' => t('Audience'),
      '#attributes' => array('class' => 'og-audience'),
      '#options' => $type == 'checkboxes' ? array_map('filter_xss', $options) : $options,
      '#required' => $required,
      '#description' =>  format_plural(count($options), 'Show this post in this group.', 'Show this post in these groups.') . $description_max_groups,
      '#default_value' => $groups ? $groups : array(),
      '#required' => $required,
      '#multiple' => TRUE);
  }
  else if ($required) {
    form_set_error('title', t('You must <a href="@join">join a group</a> before posting a %type.', array('@join' => url('og'), '%type' => node_get_types('name', $node->type))));
  }
}
// rewrite og_access_alter_nongroup_form()
function _advimage_og_access_alter_nongroup_form(&$form, $form_state, $node) {
  global $user;

  // If user has no subscriptions, don't bother with Public checkbox - it's meaningless.
  if (og_is_group_post_type($node->type) && !empty($form['og_nodeapi']['visible'])) {
    // get the visibility for normal users
    $vis = variable_get('og_visibility', 0);

    // override visibility for og admins
    if (user_access('administer organic groups')) {
      if ($vis < 2) {
        $vis = $vis == OG_VISIBLE_GROUPONLY ? OG_VISIBLE_CHOOSE_PRIVATE : OG_VISIBLE_CHOOSE_PUBLIC;
      }
    }
    elseif (!og_get_subscriptions($user->uid)) {
      // don't show checkbox if no memberships. must be public.
      $vis = OG_VISIBLE_BOTH;
    }

    // We are using this form element to communicate $groups from og to og_access.
    $groups = $form['og_initial_groups']['#value'];

    // If the post is to a private group, visibility must default to one of the private options.
    $selected_groups = isset($form_state['values']['og_groups']) ? array_filter($form_state['values']['og_groups']) : $groups;
    if (!empty($selected_groups)) {
      foreach ($selected_groups as $gid) {
        $group_node = new stdClass();
        $group_node->nid = $gid;
        og_load_group($group_node);
         if (!empty($group_node->og_private)) {
           // Set visibility to the appropriate private option.
           $vis = variable_get('og_visibility', 0) == OG_VISIBLE_GROUPONLY ? OG_VISIBLE_GROUPONLY : OG_VISIBLE_CHOOSE_PRIVATE;
          break;
        }
      }
    }
    else {
      // TODOL: No groups. Public must be checked if it is visible.
    }

		$form['og_nodeapi']['visible']['og_public'] = array(
			'#type' => 'checkbox',
			'#title' => t('Public'),
			'#default_value' => isset($node->og_public) ? $node->og_public : 0,
			'#description' => t('Show this post to everyone, or only to members of the groups checked above. Posts without any groups are always <em>public</em>.'),
			'#weight' => 2,
		);

    if (count($form['og_nodeapi']['visible']) > 1) {
      $form['og_nodeapi']['#type'] = 'fieldset';
      $form['og_nodeapi']['#title'] = t('Groups');
      $form['og_nodeapi']['#collapsible'] = TRUE;
      $form['og_nodeapi']['#collapsed'] = $selected_groups ? TRUE : FALSE;
    }

  }

}
