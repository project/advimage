<?php
/*
 * @file 
 * Module: advimage_og
 * Advanced images opengroups integration page
 * JSON Menu handlers functions
 * (c) Ilya V. Azarov, brainstorm@brainstorm.name, 2011
 */
/*
 * Call to save gallery permissions
 */
function advimage_og_edit_gallery_gjson($node) {
	global $user;
  module_load_include('inc', 'node', 'node.pages');
  drupal_set_header('Content-type: text/javascript; charset=utf-8');
  drupal_set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  drupal_set_header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header('Cache-Control: no-store, no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');
  $token_name = arg(4);
  $token = arg(5);
  $tokens_correct = FALSE;
  if ($token_name && $token) {
		$tokens_correct = drupal_valid_token($token, $token_name . '_' . $node->nid  . '_' . $node->changed);
	}
	if (!$tokens_correct) {
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Something wrong with form sessions. Try make all operations again. If error repeats tell about it to site administrator')
		) ) );
	}
	if(isset($_POST['nids'] ) 
		&& isset($_POST['og_groups_' . $node->nid] )
		&& isset($_POST['og_public_' . $node->nid] )
		&& ($_POST['nids'] == $node->nid)
	) {
		$og_public = @intval($_POST['og_public_' . $node->nid] );
		$og_groups = array();
		$_og_groups = explode(',', $_POST['og_groups_' . $node->nid] );
		$groups = og_get_subscriptions($user->uid);
		foreach($_og_groups as $gid) {
			if(is_numeric($gid) && isset($groups[$gid] ) ) {
				$og_groups[$gid] = $gid;
			}
		}
		$node->og_groups = $og_groups;
		$node->og_public = $og_public;
		node_save($node);
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'success'
		) ) );
	} else {
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Wrong request while gallery saving. Try make all operations again. If error repeats tell about it to site administrator')
		) ) );
	}
}
/*
 * Call to save permissions for an a batch of images
 */
function advimage_og_edit_gallery_ijson($node) {
	global $user;
  module_load_include('inc', 'node', 'node.pages');
  drupal_set_header('Content-type: text/javascript; charset=utf-8');
  drupal_set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  drupal_set_header('Last-Modified: '. gmdate('D, d M Y H:i:s') .' GMT');
  drupal_set_header('Cache-Control: no-store, no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');
  $token_name = arg(4);
  $token = arg(5);
  $tokens_correct = FALSE;
  if ($token_name && $token) {
		$tokens_correct = drupal_valid_token($token, $token_name . '_' . $node->nid  . '_' . $node->changed);
	}
	if (!$tokens_correct) {
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Something wrong with form sessions. Try make all operations again. If error repeats tell about it to site administrator')
		) ) );
	}
	$nids = array();
	$nids_a = array();
	if (is_array($node->field_advgallery)  && count($node->field_advgallery) ) {
		foreach($node->field_advgallery as $value) {
			if(is_numeric($value['nid'] ) ) {
				$nids []= $value['nid'];
				$nids_a[$value['nid'] ] = $value['nid'];
			}
		}
	}
	if (!count($nids) ){
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Something wrong with internal node data. Try make all operations again. If error repeats tell about it to site administrator')
		) ) );
	}
	// now we do filter an input data
	$correct_post_data = TRUE;
	$groups = og_get_subscriptions($user->uid);
	if (isset($_POST['nids'] ) ) {
		$nids_r = explode(',', '' . $_POST['nids'] );
		$nids_ra = array();
		$all_right = TRUE;
		if(count($nids_r) > ADVIMAGE_OG_IMAGES_NUM_SAVE_PER_REQUEST) {
			die(drupal_to_js(array(
				'jsonrpc' => '2.0', 'result' => 'error',
				'message' => t('Client transmit too big batch of data to the server. Try make all operations again. If error repeats tell about it to site administrator')
			) ) );
		}
		foreach ($nids_r as $nid) {
			$correct_post_data = $correct_post_data
				&& is_numeric($nid) 
				&& isset($nids_a[$nid] )
				&& isset($_POST['og_groups_' . $nid] )
				&& isset($_POST['og_public_' . $nid] );
			if (!$correct_post_data) {
				break;
			}
			// load og_groups and og_public values as an arrays there
			$og_groups = array();
			$og_public = @intval($_POST['og_public_' . $nid] );
			$_og_groups =  explode(',', $_POST['og_groups_' . $nid] );
			if (count($_og_groups) ) {
				foreach ($_og_groups as $gid) {
					if(is_numeric($gid) && isset($groups[$gid] ) ) {
						$og_groups[$gid] = $gid;
					}
				}
			}
			$nids_ra[$nid] = array('og_groups' => $og_groups, 'og_public' => $og_public);
		}
		if ($correct_post_data) {
			foreach ($nids_ra as $nid => $values) {
				if($node = node_load(array('nid' => $nid ) ) ) {
					$node->og_public = $values['og_public'];
					$node->og_groups = $values['og_groups'];
					node_save($node);
				}
			}
			die(drupal_to_js(array(
				'jsonrpc' => '2.0', 'result' => 'success',
				'data' => array('saved' => count($nids_ra) ),
			) ) );
		} else{
			die(drupal_to_js(array(
				'jsonrpc' => '2.0', 'result' => 'error',
				'message' => t('Data transmitted to server are incorrect.')
			) ) );
		}
	} else {
		die(drupal_to_js(array(
			'jsonrpc' => '2.0', 'result' => 'error',
			'message' => t('Wrong script usage')
		) ) );
	}
}
