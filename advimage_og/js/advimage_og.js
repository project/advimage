function var_dump_(arr,level) {
  var dumped_text = "";
  if(!level) level = 0;
  //The padding given at the beginning of the line.
  var level_padding = "";
  for(var j=0; j<level+1; j++) level_padding += " ";
  if(typeof(arr) == 'object') { //Array/Hashes/Objects
    for(var item in arr) {
      var value = arr[item];
      if(typeof(value) == 'object') { //If it is an array,
        dumped_text += level_padding + "'" + item + "' ...\n";
        dumped_text += var_dump_(value,level+1);
      } else {
        dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
      }
    }
  } else { //Stings/Chars/Numbers etc.
    dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
  }
  return dumped_text;
}
function var_dump(arr,level) {
  alert(var_dump_(arr,level) );
}
/*
 * @file 
 * Module: advimage_og
 * Client actions Javascript
 * (c) Ilya V. Azarov, brainstorm@brainstorm.name, 2011
 */
Drupal = Drupal || {};
Drupal.advimage_og = Drupal.advimage_og || {};
Drupal.advimage_og.formNeedSaving = false;
Drupal.advimage_og.stoppedbyerror = false;
Drupal.advimage_og.imagesweresaved = 0;
Drupal.advimage_og.signalFormNeedSavingSubmit = function(){
	if(!Drupal.advimage_og.formNeedSaving) {
		Drupal.advimage_og.formNeedSaving =  true;
		$('#advgallery-savepoint').fadeIn('slow');
	}
}
Drupal.advimage_og.openLinkInNewWindow = function(){
	window.open(this.href);
	return false;
}
Drupal.advimage_og.setSameGroups = function() {
	if($('#main-gallery-settings-form select.form-select.og-audience').val() ) {
		$('#advimage-og-edit-gallery-ingallery .advimage-og-edit-gallery-image select.form-select.og-audience').val(
			$('#main-gallery-settings-form select.form-select.og-audience').val()
		);
	}else{
		$('#advimage-og-edit-gallery-ingallery .advimage-og-edit-gallery-image select.form-select.og-audience option:selected').removeAttr('selected');
	}
	Drupal.advimage_og.signalFormNeedSavingSubmit();
	return false;
}
Drupal.advimage_og.mergeGroups = function() {
	var selected =  $('#main-gallery-settings-form select.form-select.og-audience option:selected');
	var i = 0;
	if(selected.length > 0) {
		for(i = 0; i < selected.length; i++) {
			$("#advimage-og-edit-gallery-ingallery .advimage-og-edit-gallery-image select.form-select.og-audience option[value='" + selected[i].value + "']").attr('selected', 'selected');
		}
	}
	Drupal.advimage_og.signalFormNeedSavingSubmit();
	return false;
}
Drupal.advimage_og.makeAllPublic = function() {
	$('#advimage-og-edit-gallery-ingallery .advimage-og-edit-gallery-image input.form-checkbox').attr('checked', 'checked');
	return false;
}
Drupal.advimage_og.makeAllPrivate = function() {
	$('#advimage-og-edit-gallery-ingallery .advimage-og-edit-gallery-image input.form-checkbox:checked').removeAttr('checked');
	Drupal.advimage_og.signalFormNeedSavingSubmit();
	return false;
}
Drupal.advimage_og.saveAll = function() {
	// step first. collect all the data for work.
	// 1. getting all the nodes nids
	var items = $('#advimage-og-edit-gallery-ingallery div.advimage-og-edit-gallery-image');
	var nids =new Array();
	var node_count = items.length;
	var i = 0;
	var j = 0, j1 = 0, j2 = 0;
	for (i = 0; i < node_count; i++) {
		nids[i] = items[i].id;
		nids[i] = nids[i].replace('advimage-og-edit-gallery-image-', '');
	}
	var databatches = new Array();
	var _og_groups,_og_public;
	c = Math.floor(node_count / Drupal.settings.advimage_og.og_images_num_save_per_request) 
		+ ( (node_count % Drupal.settings.advimage_og.og_images_num_save_per_request) ? 1 : 0);
  for (i =  0; i < c; i ++) {
		j1 = i * Drupal.settings.advimage_og.og_images_num_save_per_request;
		j2 = (i + 1) * Drupal.settings.advimage_og.og_images_num_save_per_request;
		if(j2 > node_count) j2 = node_count;
		var nidstr = '';
		for(j = j1; j < j2; j ++) {
			nidstr = nidstr + ( (nidstr == '') ? '' : ',') + nids[j];
		}
		databatches[i] = {nids: nidstr};
		for(j = j1; j < j2; j ++) {
			_og_groups = $('select#edit-node-og-groups-' + nids[j] ).val() ? ('' + $('select#edit-node-og-groups-' + nids[j] ).val() ) : '';
			_og_public = $('#edit-node-og-public-' + nids[j] ).attr('checked') ? 1 : 0;
		  databatches[i]['og_groups_' + nids[j] ] = _og_groups;
		  databatches[i]['og_public_' + nids[j] ] = _og_public;
		}
	}
	// databatches - sets of data for json calls to save batchs of nodes
	// 2. getting main gallery data batch :)
	// here is main nid Drupal.settings.advimage_og.advgallery_nid
	var gallerybatch = {nids : Drupal.settings.advimage_og.advgallery_nid};
	_og_groups = $('#main-gallery-settings-form select.form-select.og-audience').val() ? ('' + $('#main-gallery-settings-form select.form-select.og-audience').val() ) : '';
	_og_public = $('#main-gallery-settings-form input.form-checkbox').attr('checked') ? 1 : 0;
	gallerybatch['og_groups_' + Drupal.settings.advimage_og.advgallery_nid] = _og_groups;
	gallerybatch['og_public_' + Drupal.settings.advimage_og.advgallery_nid] = _og_public;
	$('div#advimage-og-edit-gallery-form-messages').hide();
	$('div#advimage-og-edit-gallery').hide();
	$('div#advimage-og-edit-gallery-form-messages').html(
		'<div class="saving-info">' 
			+Drupal.t('Please wait, saving all data to server.')
			+ '<br />' + Drupal.t('Saving groups and access settings for images:') + ' <span id="advimage-og-edit-status-percent"></span></div>'
	)
	$('div#advimage-og-edit-gallery-form-messages').fadeIn(30);
	// 1. saving batches
	for(i = 0; (i < databatches.length) && (!Drupal.advimage_og.stoppedbyerror); i ++) {
		$.ajax({
			url : Drupal.settings.advimage_og.advimage_batch_save_url,
			type : 'POST',
			data : databatches[i],
			success : Drupal.advimage_og.saveImagesBatchJson,
			async : false
		});
		var percent = Math.floor(100 * Drupal.advimage_og.imagesweresaved / node_count);
		$('#advimage-og-edit-status-percent').text('' + percent + '%');
	}
	// 2. saveing gallery if all is successfull
	$('div#advimage-og-edit-gallery-form-messages div.saving-info').append('<br />' + Drupal.t('Saving settings for gallery:') );
	$.ajax({
		url : Drupal.settings.advimage_og.advgallery_save_url,
		type : 'POST',
		data : gallerybatch,
		success : Drupal.advimage_og.saveGalleryJson,
		async : false
	});
	if(Drupal.advimage_og.stoppedbyerror) {
		return false;
	}
	$('div#advimage-og-edit-gallery-form-messages').append(' 100%');
	// here add an redirect to gallery url
	$(location).attr('href', Drupal.settings.advimage_og.advgallery_gallery_url);
	return false;
}
Drupal.advimage_og.showCriticalFormError = function(text) {
  if(!text) $('#advimage-og-edit-gallery-no-js-warning').text(
	  Drupal.t('Undefined error occured while processing requests to the server. Try reload this page and make all you need again')
  ); else $('#advimage-og-edit-gallery-no-js-warning').text(text);
  $('#advimage-og-edit-gallery-form-messages, #advimage-og-edit-gallery').hide();
  $('#advimage-og-edit-gallery-no-js-warning').fadeIn();
}
Drupal.advimage_og.saveImagesBatchJson = function (result) {
	var response = eval('(' + result + ')');
  if(response.result == undefined) {
		Drupal.advimage_og.showCriticalFormError(undefined);
		Drupal.advimage_og.stoppedbyerror = true;
		return
  }else{
		if(response.result == 'error') {
			Drupal.advimage_og.showCriticalFormError(response.message);
			Drupal.advimage_og.stoppedbyerror = true;
			return;
		}
		if(response.result == 'success') {
			Drupal.advimage_og.stoppedbyerror =  false;
			Drupal.advimage_og.imagesweresaved += response.data.saved;
		}
	}
}
Drupal.advimage_og.saveGalleryJson = function (result) {
	var response = eval('(' + result + ')');
  if(response.result == undefined) {
		Drupal.advimage_og.showCriticalFormError(undefined);
		Drupal.advimage_og.stoppedbyerror = true;
		return
  } else {
		if(response.result == 'error') {
			Drupal.advimage_og.showCriticalFormError(response.message);
			Drupal.advimage_og.stoppedbyerror = true;
			return;
		}
		if(response.result == 'success') {
			Drupal.advimage_og.stoppedbyerror =  false;
		}
	}
}
$(document).ready(function(){
	$('#advimage-og-edit-gallery-no-js-warning').hide();
	$('#advimage-og-edit-gallery').fadeIn(300);
	$('#advimage-og-edit-gallery-ingallery .advimage-og-edit-gallery-image a').click(
		Drupal.advimage_og.openLinkInNewWindow
	);
	$('#edit-distribute-setsamerights').click(Drupal.advimage_og.setSameGroups);
	$('#edit-distribute-mergerights').click(Drupal.advimage_og.mergeGroups);
	$('#edit-distribute-makeallpublic').click(Drupal.advimage_og.makeAllPublic);
	$('#edit-distribute-makeprivate').click(Drupal.advimage_og.makeAllPrivate);
	$('#advimage-og-edit-gallery-save-block #edit-save').click(Drupal.advimage_og.saveAll);
});
