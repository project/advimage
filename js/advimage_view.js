/**
 * $Id$
 * @file
 * JS for advanced image module, for it's gallery image view page
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
 
Drupal = Drupal || {};
Drupal.advimage = Drupal.advimage || {};
Drupal.advimage.view_openLinkInNewWindow = function(){
	window.open(this.href);
	return false;
}
$(document).ready(function(){
	$('.advimage-image-view a.fullsize-link').click(Drupal.advimage.view_openLinkInNewWindow);
});
