/**
 * @file
 * JS for advanced image module, for it's gallery edit page
 * (c) Ilya V. Azarov, 2011. Advanced image gallery module
 */
Drupal = Drupal || {};
Drupal.advimage = Drupal.advimage || {};
Drupal.advimage.ingallery_sortInWork = false;
Drupal.advimage.formNeedSaving =  false;
Drupal.advimage.last_moreimagespage = 0;
Drupal.advimage.showmoreimages_scroll = true;
Drupal.advimage.moreClickCalledByForm =  false;
/*
 * Handler to open link in new window
 */
Drupal.advimage.openLinkInNewWindow = function(){
	window.open(this.href);
	return false;
}
Drupal.advimage.clickRemoveImage = function(){
  var nid = this.rel;
  var id = '#advimage-edit-gallery-image-' + nid;
  $(id).hide();
  $(id + ' a.advimage-edit-gallery-remove-image').text(Drupal.t('Add image to gallery') );
  $(id + ' a.advimage-edit-gallery-remove-image').removeClass('advimage-edit-gallery-remove-image').addClass('advimage-edit-gallery-add-image');
  var html = $(id).html();
  $(id).remove();
  $('#advimage-edit-gallery-otherimages-int').prepend('<div id="advimage-edit-gallery-image-' + nid + '" class="advimage-edit-gallery-image">' + html + '</div>');
  Drupal.advimage.signalFormNeedSavingSubmit();
  // restoring events
  Drupal.advimage.ingallery_unbindClick();
  Drupal.advimage.ingallery_bindClicksBack();
  return false;
}

Drupal.advimage.clickAddImage =  function(){
  var nid = this.rel;
  var id = '#advimage-edit-gallery-image-' + nid;
  if(
	Drupal.settings.advimage.gallery_max_images < (
	  $('#advimage-edit-gallery-ingallery div.advimage-edit-gallery-image').length + 1 
	) 
  ) {
	alert(
	  Drupal.t('Cannot add image to gallery.') + ' ' 
	  + Drupal.t('Maximim amount of images in gallery: ') + Drupal.settings.advimage.gallery_max_images
	);
	return false;
  }
  $(id).hide();
  $(id + ' a.advimage-edit-gallery-add-image').text(Drupal.t('Remove image from gallery') );
  $(id + ' a.advimage-edit-gallery-add-image').removeClass('advimage-edit-gallery-add-image').addClass('advimage-edit-gallery-remove-image');
  var html = $(id).html();
  $(id).remove();
  $('#advimage-edit-gallery-ingallery').append('<div id="advimage-edit-gallery-image-' + nid + '" class="advimage-edit-gallery-image">' + html + '</div>');
  Drupal.advimage.signalFormNeedSavingSubmit();
  $('#advimage-edit-gallery-ingallery').animate({ scrollTop: $('#advimage-edit-gallery-ingallery').attr('scrollHeight') } );
  if($('#advimage-edit-gallery-otherimages-int div.advimage-edit-gallery-image').length < (Drupal.settings.advimage.advimage_edit_page_num / 2)) {
	Drupal.advimage.showmoreimages_scroll = false;
	Drupal.advimage.moreClickCalledByForm = true;
	Drupal.advimage.showMoreImagesClick();
	Drupal.advimage.showmoreimages_scroll = true;
	Drupal.advimage.moreClickCalledByForm = false;
  }
  // restoring events
  Drupal.advimage.ingallery_unbindClick();
  Drupal.advimage.ingallery_bindClicksBack();
  return false;
}
Drupal.advimage.signalFormNeedSavingSubmit = function(){
	if(!Drupal.advimage.formNeedSaving) {
		Drupal.advimage.formNeedSaving =  true;
		$('#advgallery-savepoint').fadeIn('slow');
	}
}
Drupal.advimage.ingallery_bindClicksBack = function(){
  $('#advimage-edit-gallery-form div.advimage-edit-gallery-image div.advimage-edit-gallery-image-preview a').click(Drupal.advimage.openLinkInNewWindow);
  $('#advimage-edit-gallery-form div.advimage-edit-gallery-image-controls a.advimage-edit-gallery-remove-image').click(Drupal.advimage.clickRemoveImage);
  $('#advimage-edit-gallery-form div.advimage-edit-gallery-image-controls a.advimage-edit-gallery-add-image').click(Drupal.advimage.clickAddImage);
}
Drupal.advimage.ingallery_unbindClick =  function() {
  $('#advimage-edit-gallery-form div.advimage-edit-gallery-image div.advimage-edit-gallery-image-preview a').unbind('click');
  $('#advimage-edit-gallery-form div.advimage-edit-gallery-image-controls a.advimage-edit-gallery-remove-image').unbind('click');
  $('#advimage-edit-gallery-form div.advimage-edit-gallery-image-controls a.advimage-edit-gallery-add-image').unbind('click');
}
Drupal.advimage.ingallery_SortStart = function (event, ui) {
  Drupal.advimage.ingallery_unbindClick();
}
Drupal.advimage.ingallery_SortStop = function (event, ui) {
  setTimeout(Drupal.advimage.ingallery_bindClicksBack, 300);
}
// displays error in noscript information block and hides form
// there are more than one same code case
Drupal.advimage.showCriticalFormError = function(text) {
  if(!text) $('#advimage-edit-gallery-form-nojavascript-warning').text(
	Drupal.t('Undefined error occured while processing requests to the server. Try reload this page and make all you need again')
  );else  $('#advimage-edit-gallery-form-nojavascript-warning').text(text);
  $('#advimage-edit-gallery-form').hide();
  $('#advimage-edit-gallery-form-nojavascript-warning').fadeIn();
}
Drupal.advimage.showMoreImagesJson = function(result){
  var response = eval('(' + result + ')');
  if(response.result == undefined) {
	Drupal.advimage.showCriticalFormError(undefined);
  }else{
	if(response.result == 'error') {
	  if(response.error_token != undefined) {
		Drupal.advimage.showCriticalFormError(response.error_token);
		return;
	  }
	  Drupal.advimage.showCriticalFormError(undefined);
	  return;
	}
	if(response.result == 'success') {
	  var oldcount = $('#advimage-edit-gallery-otherimages-int div.advimage-edit-gallery-image').length;
	  var oldheight = $('#advimage-edit-gallery-otherimages-int').attr('scrollHeight');
	  if(response.moreavailable) {
		$('#advimage-edit-gallery-otherimages-more input.form-submit').removeAttr('disabled');
	  }else{
		$('#advimage-edit-gallery-otherimages-more input.form-submit').attr('disabled','disabled');
		$('#advimage-edit-gallery-otherimages-more input.form-submit').val(
			Drupal.t('There are no images available to add to the gallery')
		);
	  }
	  if(response.imageswerereturned && (response.html != undefined) ) {
		$('#advimage-edit-gallery-otherimages-int').append(response.html);
		// and again reload events
		Drupal.advimage.ingallery_unbindClick();
		Drupal.advimage.ingallery_bindClicksBack();
		if(Drupal.advimage.showmoreimages_scroll) {
		  $('#advimage-edit-gallery-otherimages').animate({scrollTop: oldheight}, 500);
		}
	  }else{
		if(!Drupal.advimage.moreClickCalledByForm) {
		  alert(Drupal.t('Currently no more images are available to add them to this gallery') );
		}
		$('#advimage-edit-gallery-otherimages-more input.form-submit').attr('disabled','disabled');
	  }
	}
  }
}
Drupal.advimage.showMoreImagesClick = function() {
  // for gameover :)
  Drupal.advimage.last_moreimagespage ++;
  var jsonUrl = Drupal.settings.advimage.showmoreimages_json_url 
		+ '?page=' + Drupal.advimage.last_moreimagespage;
  if(!$('#advimage-edit-gallery-otherimages-more input.form-submit').attr('disabled') ) {
	$('#advimage-edit-gallery-otherimages-more input.form-submit').attr('disabled','disabled');
	$.ajax({
	  url: jsonUrl,
	  success: Drupal.advimage.showMoreImagesJson,
	  async:false
	});
  }
  return false;
}
Drupal.advimage.saveButtonClick = function() {
	// now making node list to save - getting rel from management links
  var items = $('#advimage-edit-gallery-ingallery a.advimage-edit-gallery-remove-image');
  var len = items.length;
  var nids = new Array(len);
  if(
	Drupal.settings.advimage.gallery_max_images < (
	  $('#advimage-edit-gallery-ingallery div.advimage-edit-gallery-image').length + 1 
	) 
  ) {
	alert(
	  Drupal.t('Cannot add image to gallery.') + ' ' 
	  + Drupal.t('Maximim amount of images in gallery: ') + Drupal.settings.advimage.gallery_max_images
	);
	return false;
  }
  for(var i = 0; i < len; i++) {
	  nids[i] = items[i].rel;
  }
  var str = '';
  for(var i = 0; i < len; i++) {
	  str = str + ( (i == 0) ? '' : ' ' ) + i + ':' + nids[i];
  }
  $.ajax({
	  type : "POST",
	  url : Drupal.settings.advimage.savegallery_json_url,
	  data : {'nodes' : str},
	  async : false,
	  success : Drupal.advimage.saveButtonClickJson
  });
  return false;
}
Drupal.advimage.saveButtonClickJson = function(result) {
  var response = eval('(' + result + ')');
  if(response.result == undefined) {
	Drupal.advimage.showCriticalFormError(undefined);
  }else{
	if(response.result == 'error') {
	  if(response.message) {
		Drupal.advimage.showCriticalFormError(response.message);
	  }else{
		Drupal.advimage.showCriticalFormError(undefined);
	  }
	  return;
	}
	if(response.result == 'success') {
	  $('div#advimage-edit-gallery-form-nojavascript-warning').addClass('nopadding');
	  $('div#advimage-edit-gallery-form-nojavascript-warning').html('<div id="advimage-edit-gallery-form-node-saved"></div>');
	  $('div#advimage-edit-gallery-form-node-saved').text(
		  Drupal.t('Gallery was successfully saved. Now you will be redirected to main gallery page')
	  );
	  $('#advimage-edit-gallery-form').fadeOut("fast");
	  $('div#advimage-edit-gallery-form-nojavascript-warning').fadeIn("slow");
	  $(location).attr('href', Drupal.settings.advimage.gallery_node_url);
	}
  }
}

Drupal.advimage.addbyurlClick = function () {
  var addbyurlimage= $('#edit-addbyurlimage').val();
  if('' == addbyurlimage) {
	  alert(Drupal.t('Url field is empty') );
	  return false;
  }
  if(
	Drupal.settings.advimage.gallery_max_images < (
	  $('#advimage-edit-gallery-ingallery div.advimage-edit-gallery-image').length + 1 
	) 
  ) {
	alert(
	  Drupal.t('Cannot add image to gallery.') + ' ' 
	  + Drupal.t('Maximim amount of images in gallery: ') + Drupal.settings.advimage.gallery_max_images
	);
	return false;
  }
  $.ajax({
	  type : "POST",
	  url : Drupal.settings.advimage.getimagebyurl_json_url,
	data : {'addbyurlimage' :  addbyurlimage},
	  async : false,
	  success : Drupal.advimage.addbyurlClickJson
  });
  return false;
}

Drupal.advimage.addbyurlClickJson = function(result) {
	var response =  eval('(' + result + ')');
	if(response.result == 'error') {
		alert(response.message);
		return;
	}
	if(response.result == 'success') {
		var nid = response.nid;
		if($('#advimage-edit-gallery-image-' + nid).length > 0){
			if($('#advimage-edit-gallery-ingallery #advimage-edit-gallery-image-' + nid).length > 0) {
				alert(Drupal.t('This image is already added in images list in the gallery') );
				return;
			}
			$('#advimage-edit-gallery-image-' + nid + ' a.advimage-edit-gallery-add-image').click();
			alert(Drupal.t('Image was successfully moved to gallery list') );
		}else{
		  Drupal.advimage.ingallery_unbindClick();
			$('div#advimage-edit-gallery-ingallery').append(response.html);
		  $('#advimage-edit-gallery-ingallery').animate({ scrollTop: $('#advimage-edit-gallery-ingallery').attr('scrollHeight') } );
			Drupal.advimage.ingallery_bindClicksBack();
			alert(Drupal.t('Image was successfully added to gallery list') );
		}
	}
}
$(document).ready(function(){
  $('#advimage-edit-gallery-form-nojavascript-warning').hide();
  $('#advimage-edit-gallery-ingallery').sortable( {
	  scroll : true,
	  delay : 30,
	  start : Drupal.advimage.ingallery_SortStart,
	  stop : Drupal.advimage.ingallery_SortStop,
  } );
  Drupal.advimage.ingallery_bindClicksBack();
  $('#advimage-edit-gallery-otherimages-more input.form-submit').click(Drupal.advimage.showMoreImagesClick);
  $('#addbyurlbutton-div input.form-submit').click(Drupal.advimage.addbyurlClick);
  $('#advimage-edit-gallery-savebutton input.form-submit').click(Drupal.advimage.saveButtonClick);
  $('#advimage-edit-gallery-form').fadeIn("fast");
});
